/*

    Author: Joe Finnivan
    Date:    12/4/2015
    Class:   Software Capstone CSC478C

    This app is a simple shooter.  Ships randomly appear on the left side of the screen,
    in 4 rows,and move toward the right.
    Each ship has a randomly generated speed.
    The FIRE button can be moved left to right.  The FIRE button
    launches a missile straight up, and if it hits a ship, will explode.
    Ships might drop bombs towards the missile lancher.  If a bomb
    hits the launcher, points are lost.
    Balloons are friendly, and will cause your score to reduce if you
    destroy one.  The game is timed.
    A top 10 high score list is maintained using SQLlite functions.
*/

package com.example.jjfinnivan.androidgame;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

import static android.media.AudioManager.STREAM_MUSIC;



// Top level android App class
public class MainActivity extends ActionBarActivity {

    // The game logic is all contained within this top class.
    // We use a lot of variables to keep track of state,
    // and we pre-allocate everything we possibly can.
    // We do this because the SurfaceView run method gets called
    // many times per second.  Following the normal Java
    // paradigm of creating objects when you need them
    // ends up being inefficient in this kind of environment.
    // Thus, LOTS of variables and objects are created up front.

    GameView        gv;
    Paint           drawPaint;								// The paint surface
    MissileRow      missileLine;							// The array of missiles on the screen
    ShipRow[]       rows;									// array of ship rows     
    SoundPool       soundpool;								// used for sound effects 
    CountDownTimer  countDownTimer;							// Game timer object      
    String          MyTime;									// String used to display the time remaining
    private         SparseArray<PointF> mActivePointers;	// Array used to handle multi-touch events
    Bitmap          ships[];								// array of ship bitmaps
    Bitmap          splash, boom, bigboom, bombPic;			// misc. bitmaps
    Bitmap          missilePic, gun;	                    // misc. bitmaps
    Bitmap          rarrow, larrow;							// misc. bitmaos
    highscore       hs;										// SQL object used to manage the high score list
    String          HighName; 								// High Score User name getting typed in
    String[]        HighScores;								// stores local copy of highscore list from the database
    BombList        bList;									// array of bomb objects
    SurfaceKeyboard kb;										// home-grown keyboard that works with SurfaceView

	// These define the 6 different game states	we can be in
	// They're used in the main switch statement
    final int gamemode_SPLASH_SCREEN  = 0;
    final int gamemode_MAIN_GAME      = 1;
    final int gamemode_GAME_OVER      = 2;
    final int gamemode_HELP           = 3;
    final int gamemode_GET_NAME       = 4;
    final int gamemode_HIGH_SCORES    = 5;
    int gameMode        		= gamemode_SPLASH_SCREEN;
    int nextGameMode 			= 0;


    // Define how many points each ship is worth
	// A minus number means the user looses points for hitting a friendly ship
	// REQ 1.3.3, 1.3.4
    int[] shipScores = new int[]{2, 2, -3, 3, 1, 1, 2, 1, 1, 1};
	
	// Store the speeds the bombs from each ship have
    int[] bombSpeeds = new int[]{1,1,1,1,1,1,1,1,1,1};

	// Game configuration variables
    int 		ROWCOUNT        = 4; 			// Number of rows of ships
    int 		shipFrequency   = 10; 			// How frequent new ships are created
    long 		startTime      	= 45 * 1000; 	// 45 second game time REQ 3.2.0
    long 		interval       	= 1 * 1000;     // 1 millisecond timer tick
    long 		timerLeft 		= 0;
    int    		MAX_FPS 		= 24;			// Frames per second
    int    		FRAME_PERIOD 	= 1000 / MAX_FPS;

    int         ScreenHeight    = 0;
    int         ScreenWidth     = 0;
    int         RawWidth        = 0;
    int         RawHeight       = 0;


	// Variables used to maintain game state between refreshes
    int         MyTouch         = 0;
    int         MyTouch_0       = 0;
    int         missileSpeed    = 10;
    int         missileSound    = -1;
    int         explosionSound  = -1;
    int         score           = 0;
    int         finalScore      = 0;
    boolean     running         = false;
    boolean     timerFinished   = false;
    boolean     timerRunning    = false;
    boolean     timerPaused     = false;
    int         scoreX          = 0;
    int         scoreY          = 0;;
    int         timeX           = 0;
    int         timeY           = 0;
    int         gunX            = 0;
    int         gunY            = 0;
    int         larrowX         = 0;
    int         rarrowX         = 0;
    int         fireX           = 0;
    int         buttonRow       = 0;
    int         buttonHeight    = 0;
    int         buttonWidth     = 0;
    int         helpX           = 0;
    int         HiScoresDisplayed = 0;
    int 		bmW 			= 96;
    int 		bmH 			= 96;
    int 		from 			= 0;
    int 		speedX 			= 10;
    int 		fireDown 		= 0;
    int 		firedMissile 	= 0;
    int 		HeightDiff 		= 0;
    int 		keyLock 		= 0;	
    long 		beginTime, timeDiff, sleepTime;

    //SurfaceView surfaceView;


	// The game entry point when first started up
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

		// If we're running on the emulator, speed things up
		// because it's slower than actual hardware
        if(Build.PRODUCT.matches(".*_?sdk_?.*")){
            //-- emulator --
            MAX_FPS = 30;
            FRAME_PERIOD = 1000 / MAX_FPS;
			// speed up the bomb drop
            for (int i =0; i < 10; i++) {
                bombSpeeds[i] = 3;
            }
        }
		else {
            // running on hardware
            MAX_FPS = 24;
            FRAME_PERIOD = 1000 / MAX_FPS;
			// slow the bomb drop speed
            for (int i =0; i < 10; i++) {
                bombSpeeds[i] = 1;
            }
        }

        // Create the game objects
        drawPaint       = new Paint();
        missileLine     = new MissileRow();
        rows            = new ShipRow[ROWCOUNT];
        kb              = new SurfaceKeyboard();
        hs              = new highscore(this);
        soundpool       = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
        countDownTimer  = new MyCountDownTimer(startTime, interval);
        ships           = new Bitmap[10];
        mActivePointers = new SparseArray<PointF>();
        HighName        = new String("");
        bList           = new BombList(100);

        // instantiate the ship rows
        for (int i = 0; i < ROWCOUNT; i++) {
            rows[i] = new ShipRow(bList);
        }

        // Create the game view
        gv = new GameView(this);
        this.setContentView(gv);


        // Get screen dimensions so we can adjust the bitmap sizes
        DisplayMetrics metrics  = getBaseContext().getResources().getDisplayMetrics();
        RawWidth                = metrics.widthPixels;
        RawHeight               = metrics.heightPixels;
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inScaled              = false;  // don't scale automatically

		// We use two different sizes, depending on screen size
		// Futire enhancement could allow for more fine-grained 
		// image sizing.
        if (RawWidth > 1080) {
            bmW = 96;
            bmH = 96;
        }
        else {
            bmW = 48;
            bmH = 48;
        }

        // Load the ship bitmaps, automatically scaling them
        ships[0] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.xwing32x32,o), bmW, bmH);
        ships[1] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.enterprise1, o), bmW, bmH);
        ships[2] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.balloon, o), bmW, bmH);
        ships[3] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.bwing32x32, o), bmW, bmH);
        ships[4] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.warbird1, o), bmW, bmH);
        ships[5] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.klingon, o), bmW, bmH);
        ships[6] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.borg1, o), bmW, bmH);
        ships[7] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.impshuttle32x32, o), bmW, bmH);
        ships[8] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.klingon, o), bmW, bmH);
        ships[9] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.tiefighter32x32, o), bmW, bmH);

        // load other bitmaps
        bombPic = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.bomb20x20, o), bmW/3, bmH/3);
        boom    = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.explosion1, o), bmW, bmH);
        bigboom = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.explosion64x64, o), bmW, bmH);
        gun     = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.nozzle1, o), bmW/2, bmH/2);
        missilePic = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.torpedo, o), bmW / 2, bmH / 2);
        splash  = BitmapFactory.decodeResource(getResources(), R.drawable.missile);
        rarrow  = BitmapFactory.decodeResource(getResources(), R.drawable.rarrow);
        larrow  = BitmapFactory.decodeResource(getResources(), R.drawable.larrow);


        // Load our sound files
        AssetManager assetManager = getAssets();
        setVolumeControlStream(STREAM_MUSIC);

        try {
            AssetFileDescriptor descriptor = assetManager.openFd("torpedo.mp3");
            missileSound = soundpool.load(descriptor, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            AssetFileDescriptor descriptor1 = assetManager.openFd("explosion.mp3");
            explosionSound = soundpool.load(descriptor1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

    } // end of game setup



    // Utility method thatresizes a bitmap using a matrix
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;
    }


	// We're creating a different view than the norm, so we have
	// to override some methods

    // Insert our pause and resume methods into the chain
    @Override
    protected void onPause() {
        super.onPause();
        gv.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gv.resume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    // This method handles finger touches to the screen, including multi-touch
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get pointer ID
        int pointerId = event.getPointerId(pointerIndex);

        // get masked (not specific to a pointer) action
        int maskedAction = event.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                // We have a new pointer. Lets add it to the list of pointers
                MyTouch = 1;
                PointF f = new PointF();
                f.x = event.getX(pointerIndex);
                f.y = event.getY(pointerIndex);
                mActivePointers.put(pointerId, f);
                break;
            }
            case MotionEvent.ACTION_MOVE:
            // a pointer was moved
                for (int size = event.getPointerCount(), i = 0; i < size; i++) {
                   PointF point = mActivePointers.get(event.getPointerId(i));
                   if (point != null) {
                       point.x = event.getX(pointerIndex);
                       point.y = event.getY(pointerIndex);
                   }
               }
              break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_CANCEL: {
                MyTouch = 2;
                mActivePointers.remove(pointerId);
                break;
            }

            default:
                break;
        }
        return true;
    }




    // Here is the Game View that uses a SurfaceView type
    public class GameView extends SurfaceView implements Runnable {

		// Set up a bunch of view-required methods
        Thread ViewThread = null;
        boolean threadOK  = true;
        SurfaceHolder holder;

        public GameView(Context context) {
            super(context);
            holder = this.getHolder();
        }

        public void pause() {
            threadOK = false;

            while (true) {
                try {
                    ViewThread.join();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
            ViewThread = null;
        }


        public void resume() {
            threadOK = true;
            ViewThread = new Thread(this);
            ViewThread.start();

        }


        // This SurfaceView run method gets called as many times as the 
		// operating system can manage, depending upon what else is
		// happening on the device.  Typical call rates can be 
		// upwards of 1000 per second or more.
		// Since we don't need to run at this rate, we have
		// defined a frame rate for the game to run at.
		// Once we're run for the specified frame rate, we put
		// this thread to sleep for the rmaining time so that
		// other threads running on the device can execute.
        @Override
        public void run() {
            while (threadOK) {

                if (!holder.getSurface().isValid()) {
                    continue;
                }

                Canvas gameCanvas = holder.lockCanvas();
                beginTime = System.currentTimeMillis();

                myDraw(gameCanvas);

                timeDiff = System.currentTimeMillis() - beginTime;

                // calculate sleep time to run at the selected framerate
                sleepTime = (int)(FRAME_PERIOD - timeDiff);

                if (sleepTime > 0) {
                    // if sleepTime > 0 we're OK
                    try {
                        // send the thread to sleep for a short period
                        // ALso, this is very useful for battery life
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {}
                }

                holder.unlockCanvasAndPost(gameCanvas);
            }
        }


        // The game runs in the MyDraw method.  We get called based upon the framerate defined above.
        // It's important to realize that this routine gets called between 20-30 times per
        // second, so it's important to make sure the code is optimized.  We try to pre-allocate
        // any objects we need, and to initialize as many objects as possible before the
        // game loop starts.
        // At any one time we will be in one of these game modes, specified by the gameMode variable:
        //
        //    gamemode_SPLASH_SCREEN -  Draw the splash screen and wait for a touch event
        //    gamemode_MAIN_GAME 	 - The main game loop.  This runs for 45 seconds.
		//  	  	Ships are randomly created and fly
        //       	across the screen.  Everytime the FIRE button is pressed, a torpedo gets created
        //        	and added to the torpedo list.  If the HELP button is pressed, we change gameMode
		// 			to gamemode_HELP and display the help screen, and wait for the CONTINUE button to
		//		 	be pressed to resume play.
		//	  gamemode_HELP - Displays a help screen, describing gameplay and scoring.  Pressing the
		//			CONTINUE button returns you to game play.
        //    gamemode_GAME_OVER - End of game screen. Lets you know if your score is in the top ten.
		//			pressing the CONTINUE button will either bring you back to the splach screen,
		//			or to the name entry screen, if your score made the top ten list,
		//	  gamemode_GET_NAME - THis mode displays a simple keyboard that is used to get the
		//			user's name.  THe name and score are entered into the top ten list.
		//			From here it jumps to the gamemode_HIGH_SCORES screen.
		//	  gamemode_HIGH_SCORES - This screen displays the top ten list from the SQL database.
		//			Pressing CONTINUE returns you back to the splash screen.
        //
        protected void myDraw(Canvas canvas) {


            // Everytime we enter this method, it's possible that the screen has changed, perhaps
            // from a rotation.  We always check for this, and adapt accordingly by
			// setting up all of the screen-related parameters we need for different parts
			// of the game display
            DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
            RawWidth 		= metrics.widthPixels;
            RawHeight 		= metrics.heightPixels;
            ScreenHeight 	= canvas.getHeight();
            ScreenWidth 	= canvas.getWidth();
            HeightDiff 		= RawHeight-ScreenHeight;
            int rowYstart 	= 80;
            int rowSpace 	= (ScreenHeight) / (ROWCOUNT+2);
            scoreX 			= ScreenWidth/2;
            scoreY 			= 50;
            timeX 			= (int)((float)ScreenWidth - ((float)ScreenWidth * (float).1));
            timeY 			= 50;
            gunY 			= ScreenHeight - (ScreenHeight/5);
           	larrowX 		= 0;
            rarrowX 		= (ScreenWidth/8);
            fireX   		= ScreenWidth-155;
            helpX   		= (ScreenWidth/8)*5;
            buttonRow 		= ScreenHeight-(ScreenHeight/9);
            buttonHeight 	= ScreenHeight/9;
            buttonWidth 	= ScreenWidth / 10;


			// This is the main game mode selector
            switch (gameMode) {

                //  Draw the splash screen, reset some game-related values, and wait for a touch event
				//  REQ 3.0.0
                case gamemode_SPLASH_SCREEN:
                    fireDown	= 0;
                    gunX 		= ScreenWidth / 2;
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    // draw the splash screen picture
                    canvas.drawBitmap(splash, ScreenWidth / 2 - (splash.getWidth() / 2), ScreenHeight / 2 - (splash.getHeight() / 2), drawPaint);

                    // set text characteristics
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.CENTER);
                    drawPaint.setTextSize(100);
                    drawPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
                    canvas.drawText("Missile Defense", ScreenWidth / 2, 100, drawPaint);

                    drawPaint.setTextSize(50);
                    if (running) {
                        canvas.drawText("Touch the screen to continue...", ScreenWidth / 2, ScreenHeight - 50, drawPaint);
                    } else {
                        canvas.drawText("Touch the screen to start...", ScreenWidth / 2, ScreenHeight - 50, drawPaint);
                    }

					// Make sure the touch from the previous mode is ignored					
                    if (from == 2) {
                        if (MyTouch == 2)
                            from = 0;
                    } else {
                        if (MyTouch == 1) {
                            gameMode = gamemode_MAIN_GAME;
                            break;
                        }
                    }
                    break;


                //    The main game loop.  This runs for 45 seconds.  Ships are randomly created and fly
                //    across the screen.  Everytime the FIRE button is pressed, a torpedo gets created
                //    and added to the torpedo list.  If the HELP button is pressed, we change gameMode to
				//	  gamemode_HELP, display a help screen, and wait for the CONTINUE button to be pressed to resume play
				//	  REQ 3.0.1
                case gamemode_MAIN_GAME:

                    running = true;

                    // If the timer is not running, start it up
                    if (timerRunning == false) {
                        countDownTimer.start();
                        timerRunning = true;
                        score = 0;
                    }

					// Draw the background color
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.CYAN);

                    // Locate the rows.  Do this every time in case the screen rotates
					// REQ 1.1.5
                    for (int i = 0; i < ROWCOUNT; i++) {
                        rows[i].setLocation(0, ScreenWidth, rowYstart + i * rowSpace);
                    }

					// create the randomizer
                    randomInt r = new randomInt();

                    // Update the ships and bombs for each row, adjust score accordingly
                    for (int i = 0; i < ROWCOUNT; i++) {
						// REQ 3.3.0, 3.3.1, 3.3.2
                        score += rows[i].updateShips(canvas, drawPaint, missileLine, soundpool, explosionSound, gunX, gunY);
                        score += bList.updateBombs(canvas, drawPaint, gunX, gunY, soundpool, explosionSound, gun, bigboom);
						// randomly create a new ship in the row
						// REQ 1.0.1 & 1.0.5
                        if (r.randInt(0, 500) < shipFrequency) {
                            int n = r.randInt(0, 9);
                            // Create a new ship,  give it random speed, and tell it how many points it's worth
							// REQ 1.0.4
                            rows[i].addShip(new Ship(ships[n], randomInt.randInt(5, 10), shipScores[n], boom, 50, bombSpeeds[n], bombPic));
                        }

                        // Draw the row of ships and bombs
                        rows[i].drawRow(canvas, drawPaint);
                        bList.drawBombs(canvas, drawPaint);
                    }

                    // Draw all of the missiles
                    missileLine.setLocation(gunY, 50, gunX + (gun.getWidth() / 2) - (missilePic.getWidth() / 2));
                    missileLine.updateMissiles();
                    missileLine.drawRow(canvas, drawPaint);

					// We have just updated the ships, bombs, and missiles

					// Display the controls, score, and time

                    // Display the Left arrow button
                    MyButton larrowButton = new MyButton();
                    larrowButton.setGraphicButton(larrowX, buttonRow, buttonWidth, buttonHeight, Color.GREEN, larrow);
                    larrowButton.drawButton(canvas, drawPaint);

                    // Display the Right arrow button
                    MyButton rarrowButton = new MyButton();
                    rarrowButton.setGraphicButton(rarrowX, buttonRow, buttonWidth, buttonHeight, Color.GREEN, rarrow);
                    rarrowButton.drawButton(canvas, drawPaint);

                    // Display the HELP button
                    MyButton helpButton = new MyButton();
                    helpButton.setTextButton(helpX, buttonRow, buttonWidth, buttonHeight, Color.RED, "HELP");
                    helpButton.drawButton(canvas, drawPaint);

                    // Display the FIRE button
                    MyButton fireButton = new MyButton();
                    fireButton.setTextButton(fireX, buttonRow, buttonWidth, buttonHeight, Color.RED, "FIRE");
                    fireButton.drawButton(canvas, drawPaint);

                    fireDown = 0;
					// Check the multi-touch arrays for finger presses...
                    for (int i = 0; i < mActivePointers.size(); i++) {
                        PointF point = mActivePointers.valueAt(i);

                        if (point != null) {

                            float x = point.x;
                            float y = point.y - HeightDiff;

							// Left arrow pressed
							// REQ 2.1.2
                            if (larrowButton.contains((int) x, (int) y)) {
                                if (gunX > speedX)
                                    gunX -= speedX;
							// Right arrow pressed
							// REQ 2.1.1
                            } else if (rarrowButton.contains((int) x, (int) y)) {
                                if (gunX < ScreenWidth - speedX)
                                    gunX += speedX;
							// Fire button pressed
							// REQ 2.1.3
                            } else if (fireButton.contains((int) x, (int) y)) {
                                fireDown = 1;
							// Help key pressed, change game mode
							// REQ 3.1.0
                            } else if (helpButton.contains((int) x, (int) y)) {
                                gameMode = gamemode_HELP;
                                break;
                            }
                        }
                    }

					// Act on the key presses

                    if (fireDown == 0)
                        firedMissile = 0;

					// shoot another missile
					// REQ 2.1.1, 2.2.0, 2.2.1, 2.2.3, 2.2.4
                    if (fireDown == 1 && firedMissile == 0) {
                        soundpool.play(missileSound, 1, 1, 0, 0, 1);
                        missileLine.addMissile(new Missile(missilePic, missileSpeed));
                        firedMissile = 1;
                    }

					// draw the gun based upon the latest gun position values
					// REQ 2.0.0, 2.0.1, 2.0.2, 2.1.1, 2.1.2
                    canvas.drawBitmap(gun, gunX, gunY, drawPaint);

                    // Draw the score
                    drawScore(canvas, drawPaint);

                    // Check to see if the game time has elapsed
                    // If so, go to the end screen, gameMode = gamemode_GAME_OVER
					// REQ 3.2.0
                    if (timerFinished) {
                        timerFinished = false;
                        timerRunning = false;
                        gameMode = gamemode_GAME_OVER;
                        MyTouch = 0;
                        MyTouch_0 = 0;
                        countDownTimer.cancel();
                        finalScore = score;
                    }
                    break;


		        //    gamemode_GAME_OVER - End of game screen. Lets you know if your score is in the top ten.
				//			pressing the CONTINUE button will either bring you back to the splach screen,
				//			or to the name entry screen, if your score made the top ten list,
				// RQE 3.2.0, 3.4.0, 3.4.1, 3.4.2
                case gamemode_GAME_OVER:

                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.CENTER);
                    drawPaint.setTextSize(50);

                    // Remove all ships, in case we play again
                    for (int i = 0; i < ROWCOUNT; i++) {
                        ShipRow rw = rows[i];
                        rw.clearShips();
                    }

                    // Remove all missiles, in case we play again
                    missileLine.clearMissiles();

					// Check the database to see if we're in the top ten
					// REQ 3.5.0, 3.5.1, 3.5.2
                    if (hs.checkHS(finalScore)) {
                        canvas.drawText("You made the Top Ten!!!  Your Score is " + finalScore, ScreenWidth / 2, 200, drawPaint);
						// get name for high score
						// REQ 3.5.3
                        nextGameMode = gamemode_GET_NAME;
                        score = 0;
                    }
					else {
                        canvas.drawText("You didn't make the Top Ten...  Your Score is " + finalScore, ScreenWidth / 2, 200, drawPaint);
						// Back to the splash screen, loser
						// REQ 3.4.3
                        nextGameMode = gamemode_SPLASH_SCREEN;
                        score = 0;
                    }

                    if (ContinueButton(canvas, drawPaint) == 1)
                        gameMode = nextGameMode;
                    else
                        gameMode = gamemode_GAME_OVER;
                    break;


				//	  gamemode_HELP - Displays a help screen, describing gameplay and scoring.  Pressing the
				//			CONTINUE button returns you to game play.
				// REQ 3.1.1, 3.1.2
                case gamemode_HELP:
					// Draw the help screen
                    timerPaused = true;
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.LEFT);
                    drawPaint.setTextSize(ScreenHeight/25);

                    // Set up help text position and spacing
                    int row 	  = ScreenHeight/10, col = ScreenWidth/10;
                    int lineSpace = ScreenHeight/18;

                    canvas.drawText("Press the FIRE button to launch missiles.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("Move the gun with the arrow keys.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("Hit enemy ships to score points.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("A game runs for 45 seconds.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("If you hit a balloon, you lose points.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("If a bomb hits the gun, you lose 1 point.", col, row, drawPaint);

                    lineSpace = ScreenHeight/8;
                    row += lineSpace*1.5;

                    canvas.drawBitmap(ships[2], col, row - (ships[2].getHeight() / 2), drawPaint);
                    canvas.drawText("Balloon is -2 points (you lose points)", col + ships[2].getWidth()*2, row, drawPaint);
                    row += lineSpace;
                    canvas.drawBitmap(ships[1], col, row - (ships[1].getHeight() / 2), drawPaint);
                    canvas.drawText("Enterprise is 2 points", col + ships[1].getWidth()*2, row, drawPaint);

                    row = ScreenHeight/10; col = (ScreenWidth/4) * 2;

                    canvas.drawBitmap(ships[0], col, row - (ships[0].getHeight() / 2), drawPaint);
                    canvas.drawText("X-Wing is 2 points", col + ships[0].getWidth()*2, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[7], col, row - (ships[7].getHeight() / 2), drawPaint);
                    canvas.drawText("Imperial Shuttle is 1 point", col + ships[7].getWidth()*2, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[5], col, row - (ships[5].getHeight() / 2), drawPaint);
                    canvas.drawText("Klingon is 1 point", col + ships[5].getWidth()*2, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[9], col, row - (ships[9].getHeight() / 2), drawPaint);
                    canvas.drawText("Tie Fighter is 1 point", col + ships[9].getWidth()*2, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[6], col, row - (ships[6].getHeight() / 2), drawPaint);
                    canvas.drawText("Borg Ship is 2 points", col + ships[6].getWidth()*2, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[3], col, row - (ships[3].getHeight() / 2), drawPaint);
                    canvas.drawText("B-Wing fighter is 3 points", col + ships[3].getWidth()*2, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[4], col, row - (ships[4].getHeight() / 2), drawPaint);
                    canvas.drawText("Romulan Warbird is 1 point", col + ships[4].getWidth()*2, row, drawPaint);
                    row += lineSpace;

                    if (ContinueButton(canvas, drawPaint) == 1) {
						// go back to the game
						// REQ 3.1.2
                        gameMode = 1;
                        from = 3;
                    }
                    else
                        gameMode = 3;
                    break;


				//	  gamemode_GET_NAME - THis mode displays a simple keyboard that is used to get the
				//			user's name.  THe name and score are entered into the top ten list.
				//			From here it jumps to the gamemode_HIGH_SCORES screen.
				// REQ 3.5.3
                case gamemode_GET_NAME:
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.GRAY);
                    canvas.drawText("Your name:" + HighName, ScreenWidth / 3, ScreenHeight / 6, drawPaint);
                    // Draw the keyboard
                    int keyWidth = (int)((ScreenWidth/14));
                    int keyY = ScreenHeight - keyWidth * 5;
                    int keyX = (ScreenWidth/2) - (keyWidth+5) * 6;
                    kb.drawKeyboard(keyX, keyY, keyWidth, keyWidth, canvas, drawPaint);

					// Check the key presses
                    if (mActivePointers.size() > 0) {
                        if (keyLock == 1) {
                            if (MyTouch != 2) {
                                break;
                            }
                        }
                        PointF point = mActivePointers.valueAt(0);

                        if (point != null) {
                            keyLock = 1;
                            float x = point.x;
                            float y = point.y - HeightDiff;
                            String t;
                            if ((t = kb.checkKey((int) x, (int) y)) != null) {
                                if (t.equals("SPACE")) {
                                    HighName += " ";
                                }
								// DEL key, adjust the name
                                else if (t.equals("DEL")) {
                                    if (HighName.length() > 0)
                                        HighName = HighName.substring(0, HighName.length() - 1);
                                }
								// DONE was pressed, update the SQL database
                                else if (t.equals("DONE")) {
                                    hs.dbInsert(HighName, finalScore);
                                    HiScoresDisplayed = 0;
                                    gameMode = 5;
                                    HighName = "";
                                    break;
                                }
                                else {
                                    HighName += t;
                                }
                            }
                        }
                    }
                    else {
                        keyLock = 0;
                    }
                    break;

				//	  gamemode_HIGH_SCORES - This screen displays the top ten list from the SQL database.
				//			Pressing CONTINUE returns you back to the splash screen.
				// REQ 3.5.1
                case gamemode_HIGH_SCORES:
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.GRAY);

					// Since this screen is updating many times per second,
					// we only need to read the database once.  
                    if (HiScoresDisplayed == 0) {
                        HighScores = hs.getData();
                        HiScoresDisplayed = 1;
                    }
					// set up the screen
                    int y = ScreenHeight / 6;
                    int height = ScreenHeight/30;
                    drawPaint.setColor(Color.BLUE);
                    drawPaint.setTextSize(height *3);
                    canvas.drawText("!! High Score List !!", ScreenWidth / 2, ScreenHeight / 6, drawPaint);
                    String name;
                    String score;
                    int len = HighScores.length;
                    if (len > 20)
                        len = 20;
                     drawPaint.setColor(Color.YELLOW);
                    drawPaint.setTextSize(height * 2);
					// extract the names and scores, then display them
                    for (int i = 0; i < len; i+=2) {
                        name = HighScores[i];
                        score = HighScores[i+1];
                        canvas.drawText(name + " :  "+ score, ScreenWidth / 2, y+((i+3)*height), drawPaint);
                    }

					// Wait for the CONTINUE button
					// REQ 3.5.1
                    if (ContinueButton(canvas, drawPaint) == 1)
						// back to the splash screen
						// REQ 3.4.3
                        gameMode = gamemode_SPLASH_SCREEN;
                    else
                        gameMode = 5;

                    break;

                default:
                    break;
            }
            return;
        }


		// Display the CONTINUE button, return TRUE if it is pressed
		public int ContinueButton(Canvas canvas, Paint drawPaint)
		{
		    drawPaint.setTextSize(40);
		    MyButton cButton = new MyButton();
		    int x1 = 10;
		    int width = ScreenWidth/3;
		    int height = ScreenHeight / 10;
		    int y1 = ScreenHeight - height*2;

		    cButton.setTextButton(x1, y1, width, height, Color.YELLOW, "Press here to Continue...");
		    cButton.drawButton(canvas, drawPaint);

		    for (int i = 0; i < mActivePointers.size(); i++) {
		        PointF point = mActivePointers.valueAt(i);

		        if (point != null) {

		            float x = point.x;
		            float y = point.y - HeightDiff;

		            if (cButton.contains((int) x, (int) y)) {
		                timerRunning = false;
		                from = 2;
		                return 1;
		            }
		        }
		    }
		    return 0;
		}

         // Draw the current score on the game screen
        public void drawScore(Canvas c, Paint p) {
            p.setColor(Color.BLACK);
            p.setTextAlign(Paint.Align.CENTER);
            c.drawText("Score: " + score, scoreX, scoreY, p);
            c.drawText("Time: " + MyTime, timeX, timeY, p);
        }

      }

	// The game timer
    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            timerFinished = true;
        }

        @Override
        public void onTick(long millisUntilFinished) {
                MyTime = "" + (millisUntilFinished / 1000);
            timerLeft = millisUntilFinished;
        }
    }
}

