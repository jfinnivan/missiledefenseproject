/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: MyButton


*/

package com.example.jjfinnivan.androidgame;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;



public class MyButton {
    int     x, y;   // upper left corner of the button
    int     width;  // button width in pixels
    int     height; // button height in pixels
    int     color;  // color for the button
    Bitmap  pic;    // picture to put in the button, if no ext
    String  t;      // text to put in the button
    Rect    r;      // rectangle of the button



    MyButton() {
    }


    // define the button parameters
    public void setTextButton(int x, int y, int width, int height, int color, String t) {
        this.x      = x;
        this.y      = y;
        this.width  = width;
        this.height = height;
        this.color  = color;
        this.t      = t;
        this.pic    = null;
    }

    // set the text for the button
    public void setText(int color, String t) {
        this.pic = null;
        this.color = color;
        this.t = t;

    }

    // get the button text
    public String getString()
    {
        return t;
    }


    // define a button with a picture in it
    public void setGraphicButton(int x, int y, int width, int height, int color, Bitmap pic) {
        this.x      = x;
        this.y      = y;
        this.width  = width;
        this.height = height;
        this.color  = color;
        this.pic    = pic;
        this.t      = null;
    }

    public void setParms(int x, int y, int width, int height)
    {
        this.x      = x;
        this.y      = y;
        this.width  = width;
        this.height = height;
    }

    public int getLeft() {
        return x;
    }

    public int getTop() {
        return y;
    }

    public int getBottom() {
        return y + height;
    }

    public int getRight() {
        return x + width;
    }

    // Returns true if the x,y coordinate is within the button boundaries
    public boolean contains(int x, int y) {
    if ((x >= getLeft())    &&
        (x <= getRight())   &&
        (y >= getTop())     &&
        (y <= getBottom())) {
        return true;
    } else
        return false;
    }


    // draw the button
    public void drawButton(Canvas c, Paint p) {

        p.setColor(color);
        r = new Rect(x, y, x + width, y + height);
        r.set(x, y, x + width, y + height);
        c.drawRect(r, p);

        if (pic == null) {
            p.setTextSize(height / 2);
            p.setColor(Color.BLACK);
            p.setTextAlign(Paint.Align.CENTER);
            c.drawText(t, x + (width / 2), y + (height*2) / 3, p);
        }
        else {
            c.drawBitmap(pic, x + ((width-pic.getWidth())/2), y + ((height-pic.getHeight())/2), p);
        }
    }

    public Rect getRect() {
        return r;
    }
}



