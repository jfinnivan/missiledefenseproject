/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: Ship

	This defines an individual ship.  It has an x,y position, and a speed.
	Every time a ship moves (as directed by the ShipRow class), there is a random chance it will drop a bomb (only one).
	The Ship object doesn't keep the bomb object.  It gets passed back to the ShipRow object.


*/

package com.example.jjfinnivan.androidgame;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;


public class Ship {
    int     x, y;           // location coordinates
    int     speed;          // movement increment every update
    boolean visible;        // if false, the ship isn't on the screen
    Bitmap  theShip;        // bitmap of the ship
    Bitmap  boom;           // bitap of the explosion
    Rect    r;              // bounding rectangle
    boolean boomed;         // if true, ship exploded
    int     score;          // ship value if destroyed
    int     bombRnd;        // random value that determines if a bomb should drop
    boolean droppedBomb;    // if true, the ship has dropped its bomb
    int     bombSpeed;      // bomb drop speed
    Bitmap  bombPic;        // bitmap of the bomb
    int index;

    // constructor sets things up
    Ship(Bitmap s, int speed, int score, Bitmap explosion, int bombRnd, int bombSpeed, Bitmap bombPic) {
        theShip         = s;
        this.speed      = speed;
        visible         = false;
        this.score      = score;
        r               = new Rect();
        boomed          = false;
        boom            = explosion;
        this.bombRnd    = bombRnd;
        droppedBomb     = false;
        this.bombSpeed  = bombSpeed;
        this.bombPic    = bombPic;
    }

    // set the x,y position of the ship
    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
        visible = true;
    }

    // draw the ship.  if it's "boomed", i.e, it's been exploded,
    // we draw the fireball instead of the ship
    public void draw(Canvas c, Paint p) {
        if (visible) {
            if (boomed) {
                c.drawBitmap(boom, x, y, p);
            }
            else {
                r.set(x, y, x + theShip.getWidth(), y + theShip.getHeight());
                c.drawBitmap(theShip, x, y, p);
            }
        }
    }


    // Explode the ship
    public void explode(Canvas c, Paint p) {
        c.drawBitmap(boom, x, y, p);
        boomed = true;
    }


    // Move the ship.  If it drops a bomb, we return the bomb object
    public Bomb move(int gunX, int gunY)
    {
    	// Move the ship based upon its speed
		// REQ 1.0.0 & 1.0.4, 1.0.2
        x+= speed;

        if (droppedBomb)
            return null;
        else {
            if (gunX > (x+50)) {
		    	// randomly drop a bomb
				// REQ 1.1.0 & 1.1.3, 1.2.0, 1.2.3
                if (randomInt.randInt(0, 10000) < bombRnd) {
                    droppedBomb = true;
				    // Set the bomb trajectory towards the gun position
				    // REQ 1.2.1
                    return new Bomb(bombPic, x, y, gunX, gunY, bombSpeed, boom);
                }
            }
        }
        return null;
    }

    // remove the ship
    public void remove() {
        if (!boomed)
            visible = false;
    }


    // return the x position
    public int getX() {
        return x;
    }


    // get the bounding rectangle
    public Rect getRect() {
        if (boomed)
            return null;
        else
            return r;

    }

    // The point value of this ship type
    public int getScore() {
        return score;
    }

}

