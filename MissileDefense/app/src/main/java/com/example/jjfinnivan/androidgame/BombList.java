/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: BombList

  This is a list of bomb objects. Every time its update method is 
  called,  it  will  update  each  bomb in the list. The bombList 
  always knows the current position of the gun, so when each bomb 
  is  moved,  the  list checks to see if the bomb has intersected 
  the gun. if it has, the list tells the bomb to explode.		  



*/

package com.example.jjfinnivan.androidgame;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Bitmap;
import android.media.SoundPool;


public class BombList {
    Bomb[] bombs = new Bomb[101];   // array of all the bombs
    int     start_x, start_y;       // The start position
    int     end_x, end_y;           // the end position
    int     MaxBombs = 100;         // maximum bomb count
    int     lastBombIndex;          // keeps track of last bomb in the array
    int     bombCount;              // current bomb count
    Rect    brect;

    // constructor sets up the bomb array helpers
    BombList(int maxBombs) {
        bombCount       = 0;
        lastBombIndex   = 0;
        this.MaxBombs   = maxBombs;
    }

    public int getCount()
    {
        return bombCount;
    }

    // Set the initial location values
    public void setLocation(int start_x, int starty_y, int end_x, int end_y) {
        this.start_x = start_x;
        this.start_y = start_y;
        this.end_x   = end_x;
        this.end_y   = end_y;
    }


    // draw all bombs in the list
    public void drawBombs(Canvas c, Paint p) {
        for (int i = 0; i < MaxBombs; i++) {
            if (bombs[i] != null) {
                bombs[i].draw(c, p);
            }
        }
    }

    // add a new bomb
    public void addBomb(Bomb m) {
        // The current list of bombs is checked, up to the max count, and the first empty slot is taken
        for (int i = 0; i < MaxBombs; i++) {
            if (bombs[i] == null) {
                bombs[i] = m;
                bombs[i].index = i;
                bombs[i].setPos(start_x, start_y);
                bombCount++;
                return;
            }
        }
    }

    // move each bomb.  If one falls off the end, remove it
    public int updateBombs(Canvas c, Paint p, int gunX, int gunY, SoundPool sound, int explode, Bitmap gun, Bitmap boom) {

        // scan the list and move every entry that isn't null
        for (int i = 0; i < MaxBombs; i++) {
            if (bombs[i] != null) {
                bombs[i].move();

                // if the bomb gets to ground (gun) level, remove it
                if (bombs[i].getY() >= gunY) {
                    bombs[i].remove();
                    bombCount--;
                    bombs[i] = null;
                }
                else {
                    // if the bomb is visible, check to see if we hit the gun
                    if (!bombs[i].boomed) {

                        // check to see if a bomb has hit the gun
                        // get the bomb bounding rectangle and see if it intersects the gun rectangle
						// REQ 1.2.1
                        Rect s  = bombs[i].getRect();
                        brect   = new Rect();
                        brect.set(gunX, gunY, gunX + gun.getWidth(), gunY + gun.getHeight());
                        if ((s != null) && s.intersect(brect) == true) {
                            bombs[i].explode(c, p, boom);
                            sound.play(explode, 5, 5, 0, 0, 1);
                            // subtract from the score
                            return -3;
                        }
                    }
                }
            }
        }
        return 0;
    }


    // Check to see if we have collided with the gun
    public boolean checkForCollision(Rect sr) {
        for (int i = 0; i < MaxBombs; i++) {
            if (bombs[i] != null) {
                if (Rect.intersects(sr, bombs[i].getRect())) {
                    bombs[i].remove();
                    bombs[i] = null;
                    bombCount--;
                    return true;
                }
            }
        }
        return false;
    }

    // Clear all bombs
    public void clearBombs() {
        for (int i = 0; i < MaxBombs; i++) {
            bombs[i] = null;
        }
    }

}

