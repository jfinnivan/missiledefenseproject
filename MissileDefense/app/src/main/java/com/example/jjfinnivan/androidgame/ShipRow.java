/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: ShipRow

  Keeps  track  of  all  ships in the row.
  There  are  a  variable  number of rows,
  depending on the screen height. It keeps
  track  of how many ships are in the row.
  When  a ShipRow is created, it is passed
  a reference to the bomblist object. Each
  ShipRow  can  add bombs to the bomblist.
  The  ShipRow  calls  the move method for
  each ship it its list. Each ship has its
  own speed, and can randomly drop a bomb.
  If it does drop a bomb, the ShipRow adds
  the bomb to the bomblist. When a ship is
  moved,  it  is  also checked against the
  missilerow  list  to see if any missiles
  have intersected with a ship		   

*/

package com.example.jjfinnivan.androidgame;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.SoundPool;


public class ShipRow {
    int start, end, y;                  // position information
    Ship[]      ships = new Ship[21];   // array of ships on the row, max of 20
    int         MaxShips = 20;          // max of 20 ships in a row
    int         shipCount;              // number of ships in the row
    int         lastShipIndex = 0;      // index of the last ship in the array
    BombList    bl;                     // used to reference the bomblist
    Bomb        b;                      // gets the possible bomb object when a ship moves

    // constructor is passed the bonblist
    ShipRow(BombList bl) {
        this.bl = bl;
    }

    // Set where the row is on the screen
    public void setLocation(int start, int end, int y) {
        this.start  = start;
        this.end    = end;
        this.y      = y;
        shipCount   = 0;

    }


    // Draw all ships in the row
    public void drawRow(Canvas c, Paint p) {
        for (int i = 0; i < MaxShips; i++) {
            if (ships[i] != null) {
                ships[i].draw(c, p);
            }
        }
    }


    // Add a new ship to the row
    public void addShip(Ship s) {
        // put a new ship into the first available slot in the array
        for (int i = 0; i < MaxShips; i++) {
            if (ships[i] == null) {
                s.index = i;
		// New ships start at the left side of the screen
		// REQ 1.1.1
                s.setPos(0, y);
                ships[i] = s;
                shipCount++;
                return;
            }
        }
    }


    // Call the move method for each ship
    // After a ship is moved, we compare its bounding rectange
    // with the bounding rectangles of all on-screen torpedos.
    // If any are intersecting, we explode the ship
    public int updateShips(Canvas c, Paint p, MissileRow mr, SoundPool sound, int explode, int gunX, int gunY) {
        int score = 0;
        for (int i = 0; i < MaxShips; i++) {
            // whenever a ship is moved, there's a chance it could drop a bomb
            if (ships[i] != null) {
                if ((b = ships[i].move(gunX, gunY)) != null) {
                    bl.addBomb(b);
                }
                // we're at the end of the row, so remove the ship
				// REQ 1.1.3
                if (ships[i].getX() >= end) {
                    removeShip(ships[i]);
                } else {
                    // check to see if a missile has hit the ship
				    // REQ 1.3.1, 1.3.2, 1.3.3
                    Rect s = ships[i].getRect();
                    if ((s != null) && mr.checkForCollision(s) == true) {
                        score += ships[i].getScore();
                        ships[i].explode(c, p);
                        sound.play(explode, 5, 5, 0, 0, 1);
                    }
                 }
            }
        }
        return score;
    }


    // remove the ship and update the array
    public void removeShip(Ship s) {
        ships[s.index] = null;
        s.remove();
        shipCount--;
    }

    // remove all ships
    public void clearShips() {
        for (int i = 0; i < MaxShips; i++) {
            ships[i] = null;
        }
    }
}




