/*

    Author: mjest2
    Date:   11/10/2015

    Software Capstone CSC478C

    Class: highscore

	This class interacts with the SQL database on the Android system.
	We manage a database that stores the top ten high scores with names.
*/
package com.example.jjfinnivan.androidgame;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class highscore extends SQLiteOpenHelper {

    static final String dbName = "highscores.db"; // database file name
    static final String tableName = "highscore";

    static final String nameCol = "name";  // database field names
    static final String scoreCol = "score";

    public highscore(Context context)
    {
        super(context, dbName, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String createSql = "CREATE TABLE IF NOT EXISTS highscore (" +
                "name TEXT, " +
                "score INTEGER)";
        db.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //Not used
    }

	// Used to test the database functions
    public void dbInsTest()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", "Joe");
        values.put("score", 32);
        db.insert("highscore", null, values);

    }

	// Insert a name and score into the database
    public void dbInsert(String name, int score)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("score", score);
        db.insert("highscore", null, values);


        db.close();
    }
    


	// get the entire highscore list and put it into a String array, format is name, score, name, score, etc
    public String[] getData()
    {
        ArrayList<String> results = new ArrayList();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor re = db.rawQuery("select * from highscore order by score desc limit 10", null);
            //StringBuilder sb = new StringBuilder();

            if (re != null) {
                if (re.moveToFirst()) {
                    do {
                        String n = re.getString(re.getColumnIndex("name"));
                        String sc = re.getString(re.getColumnIndex("score"));
                        results.add(n);
                        results.add(sc);
                    }
                    while (re.moveToNext());
                }
            }
        }
        catch(SQLiteException e)
        {
            SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(dbName, null);
            onCreate(db);
        }
        return results.toArray(new String[results.size()]);
    }


	// Check to see if the passed score is in the top ten
    public boolean checkHS(int score)
    {
        String[] HighScores = getData();
        int scores = HighScores.length;
        
        if (scores < 20)
            return true;

        if (scores == 0)
            return false;

        if (score > Integer.parseInt(HighScores[scores-1]))
            return true;
        else
            return false;

    }

    public Object hsGet(Object score)
    {
        int arrayTemp;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor re = db.rawQuery("select score from highscore order by score desc limit 10", null);
        ArrayList results = new ArrayList();
        if (re != null)
        {
            if(re.moveToFirst())
            {
                do
                {
                    int sc = re.getInt(re.getColumnIndex("score"));
                    results.add(sc);
                }
                while (re.moveToNext());
            }
            if (results.size() == 0)
                score = 0;
            else
                score = results.get(results.size()-1);
        }
        //score = results.get(results.size()-1);
        return score;

    }

}