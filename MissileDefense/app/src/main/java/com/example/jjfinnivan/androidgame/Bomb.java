/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: Bomb

	A Ship object randomly creates this object.
	Each ship will only create a single bomb.
	When the bomb is created, it is given a trajectory based upon
	the position of the ship that dropped it, and the position of
	the missile gun at the time the bomb is dropped. 
	That means the trajectory is fixed, so when the gun moves, the bomb won't hit it.

*/

package com.example.jjfinnivan.androidgame;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Bomb {
    int     x, y;       // position
    int     speed;      // how far the bomb moves on each update
    boolean visible;    // Once the bomb explodes or is off-screen, it's not visible
    Bitmap  theImage;   // The bomb image
    Bitmap  boom;       // The image of the explosion
    Rect    r;          // bounding rectangle used to determine if bomb hits gun
    int     gunX, gunY; // current gun position
    boolean boomed;     // if true, th bomb has exploded
    int index;

    Bomb(Bitmap pic, int x, int y, int gunX, int gunY, int speed, Bitmap boom) {
        theImage    = pic;
        this.speed  = speed;
        visible     = false;
        r           = new Rect();
        this.gunX   = gunX;
        this.gunY   = gunY;
        this.x      = x;
        this.y      = y;
        visible     = true;
        this.boom   = boom;
    }

    public void setPos(int x, int y) {
        visible = true;
    }

    // draw the bomb
    public void draw(Canvas c, Paint p) {
        if (visible) {
            c.drawBitmap(theImage, x, y, p);
            r.set(x, y, x + theImage.getWidth(), y + theImage.getHeight());
        }
    }

    // move the bomb down the screen
    public void move() {
        x           += speed;  // the x position just moves by 'speed' distance
        // the y movement is calculated using the slope
        double yinc = (double)speed/((double)(gunX-x)/(double)(gunY-y));
        y           = (int)(y+yinc);
    }

    // make the bomb invisible
    public void remove() {
        visible = false;
    }

    // explode the bomb
    public void explode(Canvas c, Paint p, Bitmap boom1) {
        c.drawBitmap(boom1, x, y, p);
        boomed = true;
    }

    // return x
    public int getX() {
        return x;
    }

    // return y
    public int getY() {
        return y;
    }

    // get the bounding rectangle
    public Rect getRect() {
        return r;
    }
}

