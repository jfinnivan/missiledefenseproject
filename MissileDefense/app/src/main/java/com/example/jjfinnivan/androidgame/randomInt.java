/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: randomInt
    This class returns a random integer between min and max, inclusive


*/
package com.example.jjfinnivan.androidgame;

import java.util.Random;

public class randomInt {


    public randomInt() {
    }

    public static int randInt(int min, int max) {

        Random rand = new Random();
        int randNum = rand.nextInt((max - min) + 1) + min;

        return randNum;
    }
}


