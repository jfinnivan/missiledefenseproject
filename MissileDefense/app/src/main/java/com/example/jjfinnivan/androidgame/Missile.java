/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: Missile
	The missile is the red ball thing that comes out of the gun and flies
	upwards towards the ships.  The missile knows its current position
	and its speed. The move method will move the missile upwards.
*/

package com.example.jjfinnivan.androidgame;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;


public class Missile {
    int     x, y;       // coordinates on the screen
    int     speed;      // movement distance per update
    boolean visible;    // if false, missile is not visible
    Bitmap  theImage;   // bitmap of the missile
    Rect    r;          // bounding rectangle used for interects
    int index;

    // constructor sets up variables
    Missile(Bitmap pic, int speed) {
        theImage    = pic;
        this.speed  = speed;
        visible     = false;
        r           = new Rect();
    }

    // set the position of the missile
    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
        visible = true;
    }

    // draw the missile
    public void draw(Canvas c, Paint p) {
        if (visible) {
            c.drawBitmap(theImage, x, y, p);
            r.set(x, y, x + theImage.getWidth(), y + theImage.getHeight());
        }
    }

    // move the missile.  We are moving up the screen, so we subtract
    public void move() {
        y -= speed;
    }

    // remove the missile
    public void remove() {
        visible = false;
    }

    public int getY() {
        return y;
    }

    // get the bounding rectangle
    public Rect getRect() {
        return r;
    }
}

