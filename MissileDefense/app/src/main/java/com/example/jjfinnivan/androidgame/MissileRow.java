/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: MissileRow

  This  object  is  a  list  of missiles. If you hit the fire key 
  multiple  times,  this list stores all of the missiles that you 
  can  see onscreen.The missilerow will move each missile it has. 
  The  checkCollision  method  in  this  object  is called by the 
  ShipRow  Object  to  see  if  any  ships have collided with the 
  missile. MyButton - This object creates a button on the screen. 
  Since  I'm  using  SurfaceView  for game speed, I can't use the 
  nice  button views Android provides. This object creates my own 
  button.  The button has a position, color, and either text or a 
  graphic  that  gets displayed in the button. It also provides a 
  "contains"  method  that tells you if the x,y point you pass is 
  contained within the button boundaries.						  



*/

package com.example.jjfinnivan.androidgame;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;



public class MissileRow {
    int     start, end, x;  // position coordinates
    Missile[] missiles = new Missile[21];
    int     MaxMissiles = 20;
    int     lastMissileIndex;
    int     missileCount;


    // constructor
    MissileRow() {
        missileCount     = 0;
        lastMissileIndex = 0;
    }


    // set the location coordinates
    public void setLocation(int start, int end, int x) {
        this.start  = start;
        this.end    = end;
        this.x      = x;
    }

    // draw all missiles in the list
    public void drawRow(Canvas c, Paint p) {
        for (int i = 0; i < MaxMissiles; i++) {
            if (missiles[i] != null) {
                missiles[i].draw(c, p);
            }
        }
    }


    // add a new missile
    public void addMissile(Missile m) {
        // put a new missile into the first available slot
        for (int i = 0; i < MaxMissiles; i++) {
            if (missiles[i] == null) {
                missiles[i] = m;
                missiles[i].index = i;
                missiles[i].setPos(x, start);
                missileCount++;
                return;
            }
        }
    }


    // move each missile.  If one falls off the end, remove it
    public void updateMissiles() {
        // walk through the list
        for (int i = 0; i < MaxMissiles; i++) {
            if (missiles[i] != null) {
                missiles[i].move();
                if (missiles[i].getY() <= end) {
                    missiles[i].remove();
                    missiles[i] = null;
                    missileCount--;
                }
            }
        }
    }


    // Check to see if we have collided with a ship
    public boolean checkForCollision(Rect sr) {
        // walk through the list of all missiles
        // If any intersect a ship, the ship explodes, and remove the missile
        for (int i = 0; i < MaxMissiles; i++) {
            if (missiles[i] != null) {
                if (Rect.intersects(sr, missiles[i].getRect())) {
                    missiles[i].remove();
                    missiles[i] = null;
                    missileCount--;
                    return true;
                }
            }
        }
        return false;
    }


    // Clear all missiles
    public void clearMissiles() {
        for (int i = 0; i < MaxMissiles; i++) {
            missiles[i] = null;
        }
    }

}

