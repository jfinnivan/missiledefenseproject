/*

    Author: Joe Finnivan
    Date:   12/4/2015

    Software Capstone CSC478C

    Class: SurfaceKeyboard

    This class draws a simple uppercase keyboard in a SurfaceView context.
    The drawKeyboard method draws the keyboard at the position specified,
    and uses the specified key width and height.
    The checkKey method checks to see if the passed x.y coordinate falls within
    any of the keys on the keyboard.

*/

package com.example.jjfinnivan.androidgame;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;


public class SurfaceKeyboard {
    MyButton [] keys = new MyButton[29];    // array of MyButtons
    int     width;                          // width of each key
    int     height;                         // height of each key
    Rect    r;                              // rectangle that borders the entire keyboard


   SurfaceKeyboard() {
       width    = 100;
       height   = 100;

       String myChars = "QWERTYUIOPASDFGHJKLZXCVBNM";

//       String c = Character.toString(myChars.charAt(1));


        // create the top row of keys
        for (int i = 0; i < 10; i++) {
            keys[i] = new MyButton();
            keys[i].setText(Color.BLUE, Character.toString(myChars.charAt(i)));
        }

        // second row of keys
        for (int i = 10; i < 19; i++) {
            keys[i] = new MyButton();
            keys[i].setText(Color.BLUE, Character.toString(myChars.charAt(i)));
        }

        // third row of keys
        for (int i = 19; i < 26; i++) {
            keys[i] = new MyButton();
            keys[i].setText(Color.BLUE, Character.toString(myChars.charAt(i)));
        }

        // Delete key
        keys[26] = new MyButton();
        keys[26].setText(Color.BLUE, "DEL");

        // space bar
        keys[27] = new MyButton();
        keys[27].setText(Color.BLUE, "SPACE");

        // done key
        keys[28] = new MyButton();
        keys[28].setText(Color.BLUE, "DONE");

   }


    // draw the keyboard
    void drawKeyboard(int x, int y, int width, int height, Canvas c, Paint p)
    {
        // draw the border rectangle first
        p.setColor(Color.DKGRAY);
        r = new Rect(x-5, y-5, x + (width+5)*12, y + (height+5)*4);
        r.set(x-5, y-5, x + (width+5)*12, y + (height+5)*4);
        c.drawRect(r, p);

        // draw the delete key
        keys[26].setParms(x + (10*(width+5)), y, width*2, height);
        keys[26].drawButton(c, p);

        // draw the first row
        for (int i = 0; i < 10; i++) {
            keys[i].setParms(x + (i * (width + 5)), y, width, height);
            keys[i].drawButton(c, p);
        }

        x += (width/2);
        y += height + 5;

        // draw the second row
        for (int i = 10; i < 19; i++) {
            keys[i].setParms(x + ((i - 10) * (width + 5)), y, width, height);
            keys[i].drawButton(c, p);
        }

        // draw the done key
        keys[28].setParms(x + (9*(width+5)), y, width*2, height);
        keys[28].drawButton(c, p);

        x += (width/2);
        y += height + 5;

        // draw the third row
        for (int i = 19; i < 26; i++) {
            keys[i].setParms(x + ((i - 19) * (width + 5)), y, width, height);
            keys[i].drawButton(c, p);
        }

        y += height + 5;

        // draw the space bar
        keys[27].setParms(x, y, (width+5)*7, height);
        keys[27].drawButton(c, p);

    }

    // check the x,y coordinate to see if it's in a key rectangle
    String checkKey(int x, int y)
    {
        for(int i = 0; i < 29; i++) {
            if (keys[i].contains(x, y))
                return keys[i].getString();
        }
        return null;

    }
}

