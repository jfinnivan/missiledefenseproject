package com.example.jjfinnivan.androidgame;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.IOException;
import java.util.Random;
import static android.media.AudioManager.STREAM_MUSIC;


/*

    Author: Joe Finnivan
    Date:    7/16/2015
    Class:     Android Apps new version

    This app is a simple shooter.  Ships randomly appear on the left side of the screen,
    in 4 rows,and move toward the right.
    Each ship has a randomly generated speed.
    When the FIRE button is pressed, a torpedo shoots towards the
    rows of ships, and if one hits a ship, the ship is destroyed.
    Each ship has a point value.  If you hit a friendly ship, you
    lose points

*/

public class MainActivity extends ActionBarActivity {

    // All the global variables we need for the app
    GameView gv;
    Random rand;
    Paint drawPaint = new Paint();
    TorpedoRow torpedoLine = new TorpedoRow();
    ShipRow[] rows = new ShipRow[4];
    SoundPool soundpool;
    CountDownTimer countDownTimer;
    String MyTime;

    // Pictures and icons
    Bitmap ships[] = new Bitmap[10];
    Bitmap splash, boom;
    Bitmap torpedoPic, gun, kirkhug, spockhappy;

    // The cscore values for each ship type.
    // We repeat some for random selection convenience
    // The ships are:
    // 0 - enterprise
    // 1 - enterprise
    // 2 - enterprise
    // 3 - enterprise
    // 4 - romulan warbird
    // 5 - klingon battler cruiser
    // 6 - borg cube
    // 7 - romulan warbird
    // 8 - klingon battler cruiser
    // 9 - borg cube
    int[] shipScores = new int[]{-3, -3, -3, -3, 1, 1, 2, 1, 1, 2};
    int gameMode = 0; // used for the main switch statement of the onDraw method
    int ROWCOUNT = 4; // Number of rows of ships
    int shipFrequency = 10; // How frequent new ships are created
    int ScreenHeight = 0;
    int ScreenWidth = 0;
    int MyTouch = 0;
    float MyTouchX = 0;
    float MyTouchY = 0;
    int MyTouch_0 = 0;
    int torpedoSpeed = 10;
    int torpedoSound = -1;
    int explosionSound = -1;
    int score = 0;
    int finalScore = 0;
    boolean running = false;
    long startTime = 45 * 1000; // 45 second game time
    long interval = 1 * 1000;     // 1 millisecond timer tick
    boolean timerFinished = false;
    boolean timerRunning = false;
    int scoreX, scoreY;
    int timeX, timeY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create the game view
        gv = new GameView(this);
        this.setContentView(gv);

        // Create the pictures
        ships[0] = BitmapFactory.decodeResource(getResources(), R.drawable.enterprise1);
        ships[1] = BitmapFactory.decodeResource(getResources(), R.drawable.enterprise1);
        ships[2] = BitmapFactory.decodeResource(getResources(), R.drawable.enterprise1);
        ships[3] = BitmapFactory.decodeResource(getResources(), R.drawable.enterprise1);
        ships[4] = BitmapFactory.decodeResource(getResources(), R.drawable.warbird1);
        ships[5] = BitmapFactory.decodeResource(getResources(), R.drawable.klingon);
        ships[6] = BitmapFactory.decodeResource(getResources(), R.drawable.borg1);
        ships[7] = BitmapFactory.decodeResource(getResources(), R.drawable.warbird1);
        ships[8] = BitmapFactory.decodeResource(getResources(), R.drawable.klingon);
        ships[9] = BitmapFactory.decodeResource(getResources(), R.drawable.borg1);
        boom = BitmapFactory.decodeResource(getResources(), R.drawable.explosion1);
        gun = BitmapFactory.decodeResource(getResources(), R.drawable.nozzle1);
        torpedoPic = BitmapFactory.decodeResource(getResources(), R.drawable.torpedo);
        splash = BitmapFactory.decodeResource(getResources(), R.drawable.enterpriseold);
        kirkhug = BitmapFactory.decodeResource(getResources(), R.drawable.kirkhug);
        spockhappy = BitmapFactory.decodeResource(getResources(), R.drawable.spockhappy);

        // instantiate the ship rows
        for (int i = 0; i < ROWCOUNT; i++) {
            rows[i] = new ShipRow();
        }

        // Load our sound files
        AssetManager assetManager = getAssets();
        setVolumeControlStream(STREAM_MUSIC);
        soundpool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);

        try {
            AssetFileDescriptor descriptor = assetManager.openFd("torpedo.mp3");
            torpedoSound = soundpool.load(descriptor, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            AssetFileDescriptor descriptor1 = assetManager.openFd("explosion.mp3");
            explosionSound = soundpool.load(descriptor1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create the game timer
        countDownTimer = new MyCountDownTimer(startTime, interval);

    }


    // Insert our pause and resume methods into the chain
    @Override
    protected void onPause() {
        super.onPause();
        gv.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gv.resume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    // Set up the touch event handler
    // Set some variables the game will check
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                MyTouch = 1;
                MyTouchX = event.getRawX();
                MyTouchY = event.getRawY();
                break;

            case MotionEvent.ACTION_UP:
                MyTouch = 2;
                break;

            default:
                //MyTouch = 0;
                break;
        }

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // The main game view
    public class GameView extends SurfaceView implements Runnable {

        Thread ViewThread = null;
        boolean threadOK = true;
        SurfaceHolder holder;

        public GameView(Context context) {
            super(context);
            holder = this.getHolder();
        }


        public void pause() {
            threadOK = false;

            while (true) {
                try {
                    ViewThread.join();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
            ViewThread = null;
        }

        public void resume() {

            threadOK = true;
            ViewThread = new Thread(this);
            ViewThread.start();

        }

        @Override
        public void run() {
            while (threadOK == true) {

                if (!holder.getSurface().isValid()) {
                    continue;
                }

                Canvas gameCanvas = holder.lockCanvas();

                // The main game method
                myDraw(gameCanvas);

                holder.unlockCanvasAndPost(gameCanvas);
            }
        }


        // Everything happens in this method.  The rate we get called is system dependent,
        // but it's usually pretty fast (1 ms update?)
        // At any one time we will be in one of these modes, specified by th gameMode variable:
        //
        //    0 Draw the splash screen and wait for a touch event
        //    1 The main game loop.  This runs for 45 seconds.  SHips are randomly created and fly
        //        across the screen.  Everytime the FIRE button is pressed, a torpedo gets created
        //        and added to the torpedo list.  If the HELP button is pressed, we change gameMode to 3
        //        display a help screen, and wait for the RESUME button to be pressed to resume play
        //    2 End of game screen. It displays the win or lose picture, and waits for a CONTINUE button
        //        press, which takes you back to the main splash screen, gameMode = 0
        //    3 Help screen.  Displays help text, and wait for the user to press the RESUME key.
        //
        protected void myDraw(Canvas canvas) {

            // Everytime we enter this method, it's possible that the screen has changed, perhaps
            // from a rotation.  We always check for this, and adapt accordingly
            ScreenHeight = canvas.getHeight();
            ScreenWidth = canvas.getWidth();
            int rowYstart = 100;
            int rowSpace = (ScreenHeight - 200) / 4;
            scoreX = ScreenWidth - 500;
            scoreY = 50;
            timeX = ScreenWidth - 200;
            timeY = 50;

            switch (gameMode) {

                //     Draw the splash screen and wait for a touch event
                case 0:
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    // draw the splash screen picture
                    canvas.drawBitmap(splash, ScreenWidth / 2 - (splash.getWidth() / 2), ScreenHeight / 2 - (splash.getHeight() / 2), drawPaint);

                    // set text characteristics
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.CENTER);
                    drawPaint.setTextSize(100);
                    canvas.drawText("The Final Frontier...", ScreenWidth / 2, 100, drawPaint);
                    drawPaint.setTextSize(50);
                    if (running) {
                        canvas.drawText("Touch the screen to continue...", ScreenWidth / 2, ScreenHeight - 50, drawPaint);
                    } else {
                        canvas.drawText("Touch the screen to start...", ScreenWidth / 2, ScreenHeight - 50, drawPaint);
                    }

                    if ((MyTouch_0 == 0) && (MyTouch == 1)) {
                        MyTouch_0 = 1;
                    }
                    if (MyTouch_0 == 1 && MyTouch == 2) {
                        MyTouch_0 = 0;
                        MyTouch = 0;
                        gameMode = 1;
                        timerRunning = false;
                    }
                    break;


                //    The main game loop.  This runs for 45 seconds.  SHips are randomly created and fly
                //    across the screen.  Everytime the FIRE button is pressed, a torpedo gets created
                //    and added to the torpedo list.  If the HELP button is pressed, we change gameMode to 3
                //    display a help screen, and wait for the RESUME button to be pressed to resume play
                case 1:
                    running = true;
                    // If the timer is not running, start it up
                    if (timerRunning == false) {
                        countDownTimer.start();
                        timerRunning = true;
                        score = 0;
                    }
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.GRAY);


                    // Locate the rows.  Do this every time in case the screen rotates
                    for (int i = 0; i < ROWCOUNT; i++) {
                        rows[i].setLocation(0, ScreenWidth, rowYstart + i * rowSpace);
                    }

                    // Update the ships for each row.

                    for (int i = 0; i < ROWCOUNT; i++) {
                        rows[i].updateShips(canvas, drawPaint);
                        if (randInt(0, 1000) < shipFrequency) {
                            int n = randInt(0, 9);
                            // Create a new ship,  give it random speed, and tell it how many points it's worth
                            rows[i].addShip(new Ship(ships[n], randInt(4, 9), shipScores[n]));
                        }
                        // Draw the row of ships
                        rows[i].drawRow(canvas, drawPaint);
                    }

                    // Display the HELP button
                    MyButton helpButton = new MyButton();
                    helpButton.setTextButton(10, ScreenHeight - 100, 150, 90, Color.RED, "HELP");
                    helpButton.drawButton(canvas, drawPaint);

                    // Display the FIRE button
                    MyButton fireButton = new MyButton();
                    fireButton.setTextButton(ScreenWidth - 200, ScreenHeight - 100, 150, 90, Color.RED, "FIRE");
                    fireButton.drawButton(canvas, drawPaint);

                    // Check for a button press
                    if ((MyTouch_0 == 0) && (MyTouch == 1)) {

                        // FIRE button has been pressed...
                        if (fireButton.contains((int) MyTouchX, (int) MyTouchY - 200)) {
                            soundpool.play(torpedoSound, 1, 1, 0, 0, 1);
                            torpedoLine.addTorpedo();
                        }
                        // HELP has been pressed.  Change to gameMode = 3
                        if (helpButton.contains((int) MyTouchX, (int) MyTouchY - 200)) {
                            gameMode = 3;
                            MyTouch = 0;
                            MyTouch_0 = 0;
                            break;
                        }
                        MyTouch_0 = 1;
                    }

                    if (MyTouch == 2) {
                        MyTouch_0 = 0;
                    }

                    // Draw all of the torpedos
                    torpedoLine.setLocation(ScreenHeight - 50, 50, ScreenWidth / 2);
                    torpedoLine.updateTorpedos();
                    torpedoLine.drawRow(canvas, drawPaint);

                    // Draw the score
                    drawScore(canvas, drawPaint);

                    // Check to see if the game time has elapsed
                    // If so, go to the end screen, gameMode = 2
                    if (timerFinished) {
                        timerFinished = false;
                        gameMode = 2;
                        MyTouch = 0;
                        MyTouch_0 = 0;
                        countDownTimer.cancel();
                        finalScore = score;
                    }
                    break;


                //    End of game screen. It displays the win or lose picture, and waits for a CONTINUE button
                //    press, which takes you back to the main splash screen, gameMode = 0
                case 2:
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.CENTER);
                    drawPaint.setTextSize(100);

                    // Remove all ships, in case we play again
                    for (int i = 0; i < ROWCOUNT; i++) {
                        ShipRow rw = rows[i];
                        rw.clearShips();
                    }

                    // Remove all torpedos, in case we play again
                    torpedoLine.clearTorpedos();

                    // Did we win or lose?
                    if (finalScore <= 0) {
                        canvas.drawBitmap(kirkhug, ScreenWidth / 2 - (splash.getWidth() / 2), ScreenHeight / 2 - (splash.getHeight() / 2), drawPaint);
                        canvas.drawText("You Lose!!!  Your Score is " + finalScore, ScreenWidth / 2, 200, drawPaint);
                        score = 0;
                    } else {
                        canvas.drawBitmap(spockhappy, ScreenWidth / 2 - (splash.getWidth() / 2), ScreenHeight / 2 - (splash.getHeight() / 2), drawPaint);
                        canvas.drawText("You Win!!!  Your Score is " + finalScore, ScreenWidth / 2, 200, drawPaint);
                        score = 0;
                    }

                    // Draw the CONTINUE button
                    drawPaint.setTextSize(50);
                    MyButton contButton = new MyButton();
                    contButton.setTextButton(50, ScreenHeight - 150, 500, 90, Color.YELLOW, "Press here to Continue...");
                    contButton.drawButton(canvas, drawPaint);

                    // If pressed, return to the splash screen
                    if ((MyTouch_0 == 0) && (MyTouch == 1)) {
                        if (contButton.contains((int) MyTouchX, (int) MyTouchY - 200)) {
                            MyTouch_0 = 1;
                        }
                    }
                    if (MyTouch_0 == 1 && MyTouch == 2) {
                        MyTouch_0 = 0;
                        MyTouch = 0;
                        gameMode = 0;
                        timerRunning = false;
                    }
                    break;


                //    Help screen.  Displays help text, and wait for the user to press the RESUME key.
                case 3:
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.LEFT);
                    drawPaint.setTextSize(50);

                    // Set up help text position and spacing
                    int row = 50, col = 700;
                    int lineSpace = 110;

                    canvas.drawText("Press the FIRE button to lauch torpedos.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("A game runs for 45 seconds.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("If you hit a friendly ship, you lose points.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("Hit enemy ships to score points.", col, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[0], col, row - (ships[0].getHeight() / 2), drawPaint);
                    canvas.drawText("Enterprise is -3 points (you lose points)", col + 150, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[6], col, row - (ships[6].getHeight() / 2), drawPaint);
                    canvas.drawText("Borg Ship is 2 points", col + 150, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[4], col, row - (ships[4].getHeight() / 2), drawPaint);
                    canvas.drawText("Romulan Warbird is 1 point", col + 150, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[5], col, row - (ships[5].getHeight() / 2), drawPaint);
                    canvas.drawText("Klingon Cruiser is 1 point", col + 150, row, drawPaint);
                    row += lineSpace;

                    // Set up the RESUME button
                    drawPaint.setTextSize(50);
                    MyButton retButton = new MyButton();
                    retButton.setTextButton(50, 50, 500, 90, Color.YELLOW, "Press here to resume...");
                    retButton.drawButton(canvas, drawPaint);

                    // Button pressed, continue playing
                    if ((MyTouch_0 == 0) && (MyTouch == 1)) {
                        if (retButton.contains((int) MyTouchX, (int) MyTouchY - 200)) {
                            MyTouch_0 = 1;
                        }
                    }
                    if (MyTouch_0 == 1 && MyTouch == 2) {
                        MyTouch_0 = 0;
                        MyTouch = 0;
                        gameMode = 1;
                    }

                    break;

                default:
                    break;
            }

            return;

        }

        // Draw the current score on the game screen
        public void drawScore(Canvas c, Paint p) {
            p.setColor(Color.BLACK);
            p.setTextAlign(Paint.Align.CENTER);
            c.drawText("Score: " + score, scoreX, scoreY, p);
            c.drawText("Time: " + MyTime, timeX, timeY, p);
        }


        // Additional support classes

        // Game timer
      }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            timerFinished = true;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            MyTime = "" + (millisUntilFinished / 1000);
        }
    }


    // MyButton class
        // This class will display a button with text
        // It provides a method that indicates if a passed in x,y coordinate falls within the button boundaries
        public class MyButton {
            int x, y;
            int width;
            int height;
            int color;
            String t;
            Bitmap b;
            Rect r;

            MyButton() {
            }

            public void setTextButton(int x, int y, int width, int height, int color, String t) {
                this.x = x;
                this.y = y;
                this.width = width;
                this.height = height;
                this.color = color;
                this.t = t;
            }

            public int getLeft() {
                return x;
            }

            public int getTop() {
                return y;
            }

            public int getBottom() {
                return y + height;
            }

            public int getRight() {
                return x + width;
            }

            // Returns true of the x,y coordinate is within the button boundaries
            public boolean contains(int x, int y) {
                if ((x >= getLeft()) &&
                        (x <= getRight()) &&
                        (y >= getTop()) &&
                        (y <= getBottom())) {
                    return true;
                } else
                    return false;
            }


            public void drawButton(Canvas c, Paint p) {
                p.setTextSize(height / 2);
                p.setColor(color);
                r = new Rect(x, y, x + width, y + height);
                r.set(x, y, x + width, y + height);
                c.drawRect(r, p);
                p.setColor(Color.BLACK);
                p.setTextAlign(Paint.Align.CENTER);
                c.drawText(t, x + (width / 2), y + (height * 2) / 3, p);

            }

            public Rect getRect() {
                return r;
            }
        }


        // This class handles an entire row of ships
        public class ShipRow {
            int start, end, y;
            Ship[] ships = new Ship[21];
            int MaxShips = 20;
            int shipCount;
            int lastShipIndex = 0;


            ShipRow() {
            }

            // Set where the row is on the screen
            public void setLocation(int start, int end, int y) {
                this.start = start;
                this.end = end;
                this.y = y;
                shipCount = 0;

            }

            // Draw all ships in the row
            public void drawRow(Canvas c, Paint p) {
                for (int i = 0; i < MaxShips; i++) {
                    if (ships[i] != null) {
                        ships[i].draw(c, p);
                    }
                }
            }

            // Add a new ship to the row
            public void addShip(Ship s) {
                for (int i = 0; i < MaxShips; i++) {
                    if (ships[i] == null) {
                        s.index = i;
                        s.setPos(0, y);
                        ships[i] = s;
                        shipCount++;
                        return;
                    }
                }
            }

            // Call the move method for each ship
            // After a ship is moved, we compare its bounding rectange
            // with the bounding rectangles of all on-screen torpedos.
            // If any are intersecting, we explode the ship
            public void updateShips(Canvas c, Paint p) {
                for (int i = 0; i < MaxShips; i++) {
                    if (ships[i] != null) {
                        ships[i].move();
                        // we're at the end of the row, so remove the ship
                        if (ships[i].getX() >= end) {
                            removeShip(ships[i]);
                        } else {
                            // check to see if a torpedo has hit the ship
                            Rect s = ships[i].getRect();
                            if ((s != null) && torpedoLine.checkForCollision(s) == true) {
                                score += ships[i].getScore();
                                ships[i].explode(c, p);
                                soundpool.play(explosionSound, 5, 5, 0, 0, 1);
                            }
                        }
                    }
                }
            }

            // remove the ship
            public void removeShip(Ship s) {
                ships[s.index] = null;
                s.remove();
                shipCount--;
            }

            // remove all ships
            public void clearShips() {
                for (int i = 0; i < MaxShips; i++) {
                    ships[i] = null;
                }
            }
        }


        // This is the ship class
        public class Ship {
            int x, y, speed;
            boolean visible;
            Bitmap theShip;
            int index = 0;
            Rect r;
            boolean boomed;
            int score;

            Ship(Bitmap s, int speed, int score) {
                theShip = s;
                this.speed = speed;
                visible = false;
                this.score = score;
                r = new Rect();
                boomed = false;
            }

            // set the x,y position of the ship
            public void setPos(int x, int y) {
                this.x = x;
                this.y = y;
                visible = true;
            }

            // draw the ship.  if it's "boomed", i.e, it's been exploded,
            // we draw the fireball instead of the ship
            public void draw(Canvas c, Paint p) {
                if (visible) {
                    if (boomed) {
                        c.drawBitmap(boom, x, y, p);
                    } else {
                        r.set(x, y, x + theShip.getWidth(), y + theShip.getHeight());
                        //c.drawRect(r, p);
                        c.drawBitmap(theShip, x, y, p);
                    }
                }
            }

            // Explode the ship
            public void explode(Canvas c, Paint p) {
                c.drawBitmap(boom, x, y, p);
                boomed = true;
            }

            // Move the ship
            public void move() {
                x += speed;
            }

            public void remove() {
                if (boomed == false)
                    visible = false;
            }

            public int getX() {
                return x;
            }

            // get the bounding rectangle
            public Rect getRect() {
                if (boomed)
                    return null;
                else
                    return r;

            }

            // The point value of this ship type
            public int getScore() {
                return score;
            }
        }


        public static int randInt(int min, int max) {

            Random rand = new Random();
            int randNum = rand.nextInt((max - min) + 1) + min;

            return randNum;
        }


        // This is the row of all torpedos
        public class TorpedoRow {
            int start, end, x;
            Torpedo[] torpedos = new Torpedo[21];
            int MaxTorpedos = 20;
            int lastTorpedoIndex;
            int torpedoCount;


            TorpedoRow() {
                torpedoCount = 0;
                lastTorpedoIndex = 0;
            }

            public void setLocation(int start, int end, int x) {
                this.start = start;
                this.end = end;
                this.x = x;
            }

            // draw all torpedos in the list
            public void drawRow(Canvas c, Paint p) {
                c.drawBitmap(gun, x - (gun.getWidth() / 4), start, p);

                for (int i = 0; i < MaxTorpedos; i++) {
                    if (torpedos[i] != null) {
                        torpedos[i].draw(c, p);
                    }
                }
            }

            // add a new torpedo
            public void addTorpedo() {
                for (int i = 0; i < MaxTorpedos; i++) {
                    if (torpedos[i] == null) {

                        torpedos[i] = new Torpedo();
                        torpedos[i].index = i;
                        torpedos[i].setPos(x, start);
                        torpedoCount++;
                        return;
                    }
                }
            }

            // move each torpedo.  If one falls off the end, remove it
            public void updateTorpedos() {
                for (int i = 0; i < MaxTorpedos; i++) {
                    if (torpedos[i] != null) {
                        torpedos[i].move();
                        if (torpedos[i].getY() <= end) {
                            torpedos[i].remove();
                            torpedos[i] = null;
                            torpedoCount--;
                        }
                    }
                }
            }

            // Check to see if we have collided with a ship
            public boolean checkForCollision(Rect sr) {
                for (int i = 0; i < MaxTorpedos; i++) {
                    if (torpedos[i] != null) {
                        if (Rect.intersects(sr, torpedos[i].getRect())) {
                            torpedos[i].remove();
                            torpedos[i] = null;
                            torpedoCount--;
                            return true;
                        }
                    }
                }
                return false;
            }

            // Clear all torpedos
            public void clearTorpedos() {
                for (int i = 0; i < MaxTorpedos; i++) {
                    torpedos[i] = null;
                }
            }

        }


        // individual torpedo class
        public class Torpedo {
            int x, y, speed;
            boolean visible;
            Bitmap theImage;
            Rect r;
            int index = 0;

            Torpedo() {
                theImage = torpedoPic;
                this.speed = torpedoSpeed;
                visible = false;
                r = new Rect();
            }

            public void setPos(int x, int y) {
                this.x = x;
                this.y = y;
                visible = true;
            }

            public void draw(Canvas c, Paint p) {
                if (visible) {
                    c.drawBitmap(theImage, x, y, p);
                    r.set(x, y, x + theImage.getWidth(), y + theImage.getHeight());
                    //c.drawRect(r, butPaint);
                }
            }

            // move the torpedo.  We are moving up the screen, so we subtract
            public void move() {
                y -= speed;
            }

            public void remove() {
                visible = false;
            }

            public int getY() {
                return y;
            }

            public Rect getRect() {
                return r;
            }
        }

    }


