package com.example.jjfinnivan.androidgame;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

	Button class

*/
// MyButton class
// This class will display a button with text
// It provides a method that indicates if a passed in x,y coordinate falls within the button boundaries
public class MyButton {
	int x, y;
    int width;
    int height;
    int color;
    Bitmap pic;
    String t;
    Rect r;

    MyButton() {
    }

    public void setTextButton(int x, int y, int width, int height, int color, String t) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.t = t;
        this.pic = null;
    }

    public void setText(int color, String t) {
        this.pic = null;
        this.color = color;
        this.t = t;

    }
    public String getString()
    {
        return t;
    }

    public void setGraphicButton(int x, int y, int width, int height, int color, Bitmap pic) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.pic = pic;
        this.t = null;
    }

    public void setParms(int x, int y, int width, int height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getLeft() {
    return x;
    }

    public int getTop() {
    return y;
    }

    public int getBottom() {
    return y + height;
    }

    public int getRight() {
    return x + width;
    }

    // Returns true of the x,y coordinate is within the button boundaries
    public boolean contains(int x, int y) {
    if ((x >= getLeft()) &&
    	(x <= getRight()) &&
        (y >= getTop()) &&
        (y <= getBottom())) {
        return true;
    } else
        return false;
    }


    public void drawButton(Canvas c, Paint p) {

        p.setColor(color);
        r = new Rect(x, y, x + width, y + height);
        r.set(x, y, x + width, y + height);
        c.drawRect(r, p);

        if (pic == null) {
            p.setTextSize(height / 2);
            p.setColor(Color.BLACK);
            p.setTextAlign(Paint.Align.CENTER);
            c.drawText(t, x + (width / 2), y + (height*2) / 3, p);
        }
        else {
            c.drawBitmap(pic, x + ((width-pic.getWidth())/2), y + ((height-pic.getHeight())/2), p);
        }
    }

    public Rect getRect() {
    return r;
    }
}



