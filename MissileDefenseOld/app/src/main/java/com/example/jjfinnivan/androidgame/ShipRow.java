package com.example.jjfinnivan.androidgame;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.SoundPool;
/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

	Ship row class

*/

public class ShipRow {
    int start, end, y;
    Ship[] ships = new Ship[21];
    int MaxShips = 20;
    int shipCount;
    int lastShipIndex = 0;
    BombList bl;
    Bomb b;

    ShipRow(BombList bl) {
        this.bl = bl;
    }

    // Set where the row is on the screen
    public void setLocation(int start, int end, int y) {
        this.start = start;
        this.end = end;
        this.y = y;
        shipCount = 0;

    }

    // Draw all ships in the row
    public void drawRow(Canvas c, Paint p) {
        for (int i = 0; i < MaxShips; i++) {
            if (ships[i] != null) {
                ships[i].draw(c, p);
            }
        }
    }

    // Add a new ship to the row
    public void addShip(Ship s) {
        for (int i = 0; i < MaxShips; i++) {
            if (ships[i] == null) {
                s.index = i;
                s.setPos(0, y);
                ships[i] = s;
                shipCount++;
                return;
            }
        }
    }

    // Call the move method for each ship
    // After a ship is moved, we compare its bounding rectange
    // with the bounding rectangles of all on-screen torpedos.
    // If any are intersecting, we explode the ship
    public int updateShips(Canvas c, Paint p, MissileRow mr, SoundPool sound, int explode, int gunX, int gunY) {
        int score = 0;
        for (int i = 0; i < MaxShips; i++) {
            if (ships[i] != null) {
                if ((b = ships[i].move(gunX, gunY)) != null) {
                    bl.addBomb(b);
                }
                // we're at the end of the row, so remove the ship
                if (ships[i].getX() >= end) {
                    removeShip(ships[i]);
                } else {
                    // check to see if a torpedo has hit the ship
                    Rect s = ships[i].getRect();
                    if ((s != null) && mr.checkForCollision(s) == true) {
                        score += ships[i].getScore();
                        ships[i].explode(c, p);
                        sound.play(explode, 5, 5, 0, 0, 1);
                    }
                 }
            }
        }
        return score;
    }

    // remove the ship
    public void removeShip(Ship s) {
        ships[s.index] = null;
        s.remove();
        shipCount--;
    }

    // remove all ships
    public void clearShips() {
        for (int i = 0; i < MaxShips; i++) {
            ships[i] = null;
        }
    }
}




