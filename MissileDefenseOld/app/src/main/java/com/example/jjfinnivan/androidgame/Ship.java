package com.example.jjfinnivan.androidgame;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

	Ship class

*/

public class Ship {
	int x, y, speed;
    boolean visible;
    Bitmap theShip;
    Bitmap boom;
    int index = 0;
    Rect r;
    boolean boomed;
    int score;
    int bombRnd;
    boolean droppedBomb;
    int bombSpeed;
    Bitmap bombPic;


    Ship(Bitmap s, int speed, int score, Bitmap explosion, int bombRnd, int bombSpeed, Bitmap bombPic) {
        theShip = s;
        this.speed = speed;
        visible = false;
        this.score = score;
        r = new Rect();
        boomed = false;
        boom = explosion;
        this.bombRnd = bombRnd;
        droppedBomb = false;
        this.bombSpeed = bombSpeed;
        this.bombPic = bombPic;
    }

    // set the x,y position of the ship
    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
        visible = true;
    }

    // draw the ship.  if it's "boomed", i.e, it's been exploded,
    // we draw the fireball instead of the ship
    public void draw(Canvas c, Paint p) {
        if (visible) {
            if (boomed) {
                c.drawBitmap(boom, x, y, p);
            } else {
                r.set(x, y, x + theShip.getWidth(), y + theShip.getHeight());
                //c.drawRect(r, p);
                c.drawBitmap(theShip, x, y, p);
            }
        }
    }


    // Explode the ship
    public void explode(Canvas c, Paint p) {
        c.drawBitmap(boom, x, y, p);
        boomed = true;
    }

    // Move the ship
    public Bomb move(int gunX, int gunY)
    {
        x+= speed;
        if (droppedBomb)
            return null;
        else {
            if (gunX > (x+50)) {
                if (randomInt.randInt(0, 10000) < bombRnd) {
                    droppedBomb = true;
                    return new Bomb(bombPic, x, y, gunX, gunY, bombSpeed, boom);
                }
            }

        }
        return null;
    }

    public void remove() {
        if (!boomed)
            visible = false;
    }

    public int getX() {
        return x;
    }

    // get the bounding rectangle
    public Rect getRect() {
        if (boomed)
            return null;
        else
            return r;

    }

    // The point value of this ship type
    public int getScore() {
        return score;
    }
}

