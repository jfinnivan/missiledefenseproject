package com.example.jjfinnivan.androidgame;

/**
 * Created by mjest2 on 11/10/2015.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class highscore extends SQLiteOpenHelper {

    static final String dbName = "highscores.db";
    static final String tableName = "highscore";

    static final String nameCol = "name";
    static final String scoreCol = "score";

    public highscore(Context context)
    {
        super(context, dbName, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String createSql = "CREATE TABLE IF NOT EXISTS highscore (" +
                //"id INTEGER PRIMARY KEY, " +
                "name TEXT, " +
                "score INTEGER)";
        db.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //Not used
    }

    public void dbInsert(String name, int score)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Object compare = hsGet(score);
        int comp = (int) compare;

        if(comp < score) {
            //remLowestScore(score);
            ContentValues values = new ContentValues();
            values.put(nameCol, name);
            values.put(scoreCol, score);
            db.insert(dbName, null, values);
        }

        db.close();
    }
    public Object getData()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor re = db.rawQuery("select * from highscore order by score desc limit 10", null);
        //StringBuilder sb = new StringBuilder();
        ArrayList results = new ArrayList();
        if (re != null)
        {
            if (re.moveToFirst())
            {
                do
                {
                    String n = re.getString(re.getColumnIndex("name"));
                    int sc = re.getInt(re.getColumnIndex("score"));
                    results.add(n);
                    results.add(sc);
                }
                while (re.moveToNext());
            }
        }

        return results;
    }

    //Get lowest highscore of the database to compare to new score
    public Object hsGet(Object score)
    {
        int arrayTemp;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor re = db.rawQuery("select score from highscore order by score desc limit 10", null);
        ArrayList results = new ArrayList();
        if (re != null)
        {
            if(re.moveToFirst())
            {
                do
                {
                    int sc = re.getInt(re.getColumnIndex("score"));
                    results.add(sc);
                }
                while (re.moveToNext());
            }
        }
        score = results.get(results.size()-1);
        return score;

    }
    /*
    public void remLowestScore(int score)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        String remQuery = "delete from highscore where score =" + score;
        db.execSQL(remQuery);
    }
    */

}