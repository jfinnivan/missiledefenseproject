package com.example.jjfinnivan.androidgame;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.Random;

import static android.media.AudioManager.STREAM_MUSIC;



/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

    This app is a simple shooter.  Ships randomly appear on the left side of the screen,
    in 4 rows,and move toward the right.
    Each ship has a randomly generated speed.
    The FIRE button can be moved left to right.  The FIRE button
    launches a missile straight up, and if it hits a ship, will explode.
    Ships might drop bombs towards the missile lancher.  If a bomb
    hits the launcher, it is destroyed.  You get 3 launchers.
    Balloons are friendly, and will cause your score to reduce if you
    destroy one.  The game is timed.

*/

public class MainActivity extends ActionBarActivity {
    //public class MainActivity extends Activity implements SurfaceHolder.Callback {
    // All the global variables we need for the app
    GameView gv;
    Random rand;
    Paint drawPaint = new Paint();
    MissileRow missileLine = new MissileRow();
    ShipRow[] rows = new ShipRow[4];
    SoundPool soundpool;
    CountDownTimer countDownTimer;
    String MyTime;
    private SparseArray<PointF> mActivePointers;
    private static final int SIZE = 60;

    private int[] colors = { Color.BLUE, Color.GREEN, Color.MAGENTA,
            Color.BLACK, Color.CYAN, Color.GRAY, Color.RED, Color.DKGRAY,
            Color.LTGRAY, Color.YELLOW };

    // Pictures and icons
    Bitmap ships[] = new Bitmap[10];
    Bitmap splash, boom, bigboom, bombPic;
    Bitmap missilePic, gun, kirkhug, spockhappy;
    Bitmap rarrow, larrow;
    highscore hs;

    // The score values for each ship type.
    // We repeat some for random selection convenience
    // The ships are:
    // 0 - balloon
    // 1 - balloon
    // 2 - balloon
    // 3 - balloon
    // 4 - romulan warbird
    // 5 - klingon battler cruiser
    // 6 - borg cube
    // 7 - romulan warbird
    // 8 - klingon battler cruiser
    // 9 - borg cube

    // Define how many points each ship is worth
    int[] shipScores = new int[]{1, 2, -3, 2, 1, 1, 2, 1, 1, 2};
    int[] bombSpeeds = new int[]{5,5,5,5,5,5,5,5,5,5};

    // The number of ship rows
    int ROWCOUNT        = 3; // Number of rows of ships
    int shipFrequency   = 5; // How frequent new ships are created
    long startTime      = 30 * 1000; // 45 second game time
    long interval       = 1 * 1000;     // 1 millisecond timer tick
    long timerLeft = 0;

    int         gameMode        = 0; // used for the main switch statement of the onDraw method
    int         nextGameMode = 0;
    int         ScreenHeight    = 0;
    int         ScreenWidth     = 0;
    int         RawWidth        = 0;
    int         RawHeight       = 0;
    int         MyTouch         = 0;
    float       MyTouchX        = 0;
    float       MyTouchY        = 0;
    int         MyTouch_0       = 0;
    int         missileSpeed    = 10;
    int         missileSound    = -1;
    int         explosionSound  = -1;
    int         score           = 0;
    int         finalScore      = 0;
    boolean 	running         = false;
    boolean 	timerFinished   = false;
    boolean 	timerRunning    = false;
    boolean     timerPaused     = false;
    int         scoreX          = 0;
    int         scoreY          = 0;;
    int         timeX           = 0;
    int         timeY           = 0;
    int         gunX            = 0;
    int         gunY            = 0;
    int         larrowX         = 0;
    int         rarrowX         = 0;
    int         fireX           = 0;
    int         buttonRow       = 0;
    int         buttonHeight    = 0;
    int         buttonWidth     = 0;
    int         helpX           = 0;
    int bmW = 96;
    int bmH = 96;
    String HighName;
    int from = 0;
    int speedX = 10;
    int fireDown = 0;
    int buttons = 0;
    int firedMissile = 0;
    int HeightDiff = 0;
    int keyLock = 0;
    BombList bList;
    long beginTime, timeDiff, sleepTime;
    int framesSkipped;
    private final static int    MAX_FPS = 30;
    private final static int    MAX_FRAME_SKIPS = 5;
    private final static int    FRAME_PERIOD = 1000 / MAX_FPS;
    TextView textViewEdit;
    LayoutParams params;
    LayoutParams testbox;
    SurfaceView surfaceView;
    LinearLayout layout;
    SurfaceHolder surfaceHolder;
    EditText editField;
    SurfaceKeyboard kb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        setContentView(R.layout.activity_main);
        //surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        //surfaceHolder = surfaceView.getHolder();
        //surfaceHolder.addCallback(this);

        kb = new SurfaceKeyboard();
        hs = new highscore(this);

        editField = (EditText) findViewById(R.id.editText2);

        // Create the game view
        gv = new GameView(this);
        this.setContentView(gv);

        mActivePointers = new SparseArray<PointF>();

        DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
        RawWidth = metrics.widthPixels;
        RawHeight = metrics.heightPixels;

        BitmapFactory.Options o=new BitmapFactory.Options();
        o.inScaled = false;

        if (RawWidth > 1080) {
            bmW = 96;
            bmH = 96;
        }
        else {
            bmW = 48;
            bmH = 48;
        }

HighName = new String("");

    ships[0] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.xwing32x32,o), bmW, bmH);
        ships[1] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.enterprise1, o), bmW, bmH);
        ships[2] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.balloon, o), bmW, bmH);
        ships[3] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.bwing32x32, o), bmW, bmH);
        ships[4] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.warbird1, o), bmW, bmH);
        ships[5] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.klingon, o), bmW, bmH);
        ships[6] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.borg1, o), bmW, bmH);
        ships[7] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.impshuttle32x32, o), bmW, bmH);
        ships[8] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.klingon, o), bmW, bmH);
        ships[9] = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.tiefighter32x32, o), bmW, bmH);
        bombPic = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.bomb20x20, o), bmW/3, bmH/3);
        boom = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.explosion1, o), bmW, bmH);
        bigboom = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.explosion64x64, o), bmW, bmH);
        gun = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.nozzle1, o), bmW/2, bmH/2);
        missilePic = getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.torpedo, o), bmW / 2, bmH / 2);
        splash = BitmapFactory.decodeResource(getResources(), R.drawable.missile);


        rarrow = BitmapFactory.decodeResource(getResources(), R.drawable.rarrow);
        larrow = BitmapFactory.decodeResource(getResources(), R.drawable.larrow);

        bList = new BombList(100);
        // instantiate the ship rows
        for (int i = 0; i < ROWCOUNT; i++) {
            rows[i] = new ShipRow(bList);
        }

        // Load our sound files
        AssetManager assetManager = getAssets();
        setVolumeControlStream(STREAM_MUSIC);
        soundpool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);

        countDownTimer = new MyCountDownTimer(startTime, interval);

        try {
            AssetFileDescriptor descriptor = assetManager.openFd("torpedo.mp3");
            missileSound = soundpool.load(descriptor, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            AssetFileDescriptor descriptor1 = assetManager.openFd("explosion.mp3");
            explosionSound = soundpool.load(descriptor1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create the pictures
         // Create the game timer
        //countDownTimer = new MyCountDownTimer(startTime, interval);

    }

     public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        //bm.recycle();
        return resizedBitmap;
    }

    // Insert our pause and resume methods into the chain
    @Override
    protected void onPause() {
        super.onPause();
        gv.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gv.resume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get pointer ID
        int pointerId = event.getPointerId(pointerIndex);

        // get masked (not specific to a pointer) action
        int maskedAction = event.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                // We have a new pointer. Lets add it to the list of pointers
                MyTouch = 1;
                PointF f = new PointF();
                f.x = event.getX(pointerIndex);
                f.y = event.getY(pointerIndex);
                mActivePointers.put(pointerId, f);
                break;
            }
            case MotionEvent.ACTION_MOVE:
            // a pointer was moved
                for (int size = event.getPointerCount(), i = 0; i < size; i++) {
                   PointF point = mActivePointers.get(event.getPointerId(i));
                   if (point != null) {
                       point.x = event.getX(pointerIndex);
                       point.y = event.getY(pointerIndex);
                   }
               }
              break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_CANCEL: {
                MyTouch = 2;
                mActivePointers.remove(pointerId);
                break;
            }

            default:
                //MyTouch = 0;
                break;

        }
        //invalidate();

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // The main game view
    public class GameView extends SurfaceView implements Runnable {

        Thread ViewThread = null;
        boolean threadOK = true;
        SurfaceHolder holder;

        public GameView(Context context) {
            super(context);
            holder = this.getHolder();
        }


        public void pause() {
            threadOK = false;

            while (true) {
                try {
                    ViewThread.join();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
            ViewThread = null;
        }

        public void resume() {

            threadOK = true;
            ViewThread = new Thread(this);
            ViewThread.start();

        }


        // This calls the main game routine as fast as the device can manage
        @Override
        public void run() {
            while (threadOK) {

                if (!holder.getSurface().isValid()) {
                    continue;
                }


                Canvas gameCanvas = holder.lockCanvas();
                beginTime = System.currentTimeMillis();

                myDraw(gameCanvas);

                timeDiff = System.currentTimeMillis() - beginTime;

                // calculate sleep time
                sleepTime = (int)(FRAME_PERIOD - timeDiff);

                if (sleepTime > 0) {
                    // if sleepTime > 0 we're OK
                    try {
                        // send the thread to sleep for a short period
                        // very useful for battery saving
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {}
                }

                holder.unlockCanvasAndPost(gameCanvas);
            }
        }


        // Everything happens in this method.  The rate we get called is system dependent,
        // but it's usually pretty fast (1 ms update?)
        // At any one time we will be in one of these modes, specified by th gameMode variable:
        //
        //    0 Draw the splash screen and wait for a touch event
        //    1 The main game loop.  This runs for 45 seconds.  SHips are randomly created and fly
        //        across the screen.  Everytime the FIRE button is pressed, a torpedo gets created
        //        and added to the torpedo list.  If the HELP button is pressed, we change gameMode to 3
        //        display a help screen, and wait for the RESUME button to be pressed to resume play
        //    2 End of game screen. It displays the win or lose picture, and waits for a CONTINUE button
        //        press, which takes you back to the main splash screen, gameMode = 0
        //    3 Help screen.  Displays help text, and wait for the user to press the RESUME key.
        //
        protected void myDraw(Canvas canvas) {

            // Everytime we enter this method, it's possible that the screen has changed, perhaps
            // from a rotation.  We always check for this, and adapt accordingly

            DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
            RawWidth = metrics.widthPixels;
            RawHeight = metrics.heightPixels;

            ScreenHeight = canvas.getHeight();
            ScreenWidth = canvas.getWidth();
            HeightDiff = RawHeight-ScreenHeight;

            int rowYstart = 80;
            int rowSpace = (ScreenHeight) / (ROWCOUNT+2);
            scoreX = ScreenWidth/2;
            scoreY = 50;
            timeX = (int)((float)ScreenWidth - ((float)ScreenWidth * (float).1));
            timeY = 50;
            gunY = ScreenHeight - (ScreenHeight/5);
           larrowX = 0; //(ScreenWidth/8);
            rarrowX = (ScreenWidth/8);
            fireX   = ScreenWidth-155;
            helpX   = (ScreenWidth/8)*5;
            buttonRow = ScreenHeight-(ScreenHeight/9);
            buttonHeight = ScreenHeight/9;
            buttonWidth = ScreenWidth / 10;



            switch (gameMode) {

                //     Draw the splash screen and wait for a touch event
                case 0:
                    fireDown = 0;
                    gunX = ScreenWidth / 2; // This will change
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    // draw the splash screen picture
                    canvas.drawBitmap(splash, ScreenWidth / 2 - (splash.getWidth() / 2), ScreenHeight / 2 - (splash.getHeight() / 2), drawPaint);

                    // set text characteristics
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.CENTER);
                    drawPaint.setTextSize(100);
                    drawPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
                    //drawPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));

                    canvas.drawText("Missile Defense", ScreenWidth / 2, 100, drawPaint);
                    drawPaint.setTextSize(50);
                    if (running) {
                        canvas.drawText("Touch the screen to continue...", ScreenWidth / 2, ScreenHeight - 50, drawPaint);
                    } else {
                        canvas.drawText("Touch the screen to start...", ScreenWidth / 2, ScreenHeight - 50, drawPaint);
                    }

                    //Rect tr = new Rect(5, 5, ScreenWidth-5, ScreenHeight-5);
                    //drawPaint.setColor(Color.BLUE);
                    //canvas.drawRect(tr, drawPaint);

                    if (from == 2) {
                        if (MyTouch == 2)
                            from = 0;
                    } else {
                        if (MyTouch == 1) {
                            gameMode = 1;
                            break;
                        }
                    }
                    break;


                //    The main game loop.  This runs for 45 seconds.  SHips are randomly created and fly
                //    across the screen.  Everytime the FIRE button is pressed, a torpedo gets created
                //    and added to the torpedo list.  If the HELP button is pressed, we change gameMode to 3
                //    display a help screen, and wait for the RESUME button to be pressed to resume play
                case 1:

                    running = true;
                    //if (from == 3) {
                    //    countDownTimer = new MyCountDownTimer(timerLeft, interval);
//
//                    }

                    // If the timer is not running, start it up
                    if (timerRunning == false) {
                        countDownTimer.start();
                        timerRunning = true;
                        score = 0;
                    }

                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.GRAY);


                    // Locate the rows.  Do this every time in case the screen rotates
                    for (int i = 0; i < ROWCOUNT; i++) {
                        rows[i].setLocation(0, ScreenWidth, rowYstart + i * rowSpace);
                    }

                    // Update the ships for each row.
                    randomInt r = new randomInt();

                    for (int i = 0; i < ROWCOUNT; i++) {
                        score += rows[i].updateShips(canvas, drawPaint, missileLine, soundpool, explosionSound, gunX, gunY);
                        bList.updateBombs(canvas, drawPaint, gunX, gunY, gun, bigboom);
                        if (r.randInt(0, 500) < shipFrequency) {
                            int n = r.randInt(0, 9);
                            // Create a new ship,  give it random speed, and tell it how many points it's worth
                            rows[i].addShip(new Ship(ships[n], randomInt.randInt(5, 10), shipScores[n], boom, 100, bombSpeeds[n], bombPic));
                        }
                        // Draw the row of ships
                        rows[i].drawRow(canvas, drawPaint);
                        bList.drawBombs(canvas, drawPaint);
                    }


                    // Display the Left arrow button
                    MyButton larrowButton = new MyButton();
                    larrowButton.setGraphicButton(larrowX, buttonRow, buttonWidth, buttonHeight, Color.GREEN, larrow);
                    larrowButton.drawButton(canvas, drawPaint);

                    // Display the Right arrow button
                    MyButton rarrowButton = new MyButton();
                    rarrowButton.setGraphicButton(rarrowX, buttonRow, buttonWidth, buttonHeight, Color.GREEN, rarrow);
                    rarrowButton.drawButton(canvas, drawPaint);


                    // Display the HELP button
                    MyButton helpButton = new MyButton();
                    helpButton.setTextButton(helpX, buttonRow, buttonWidth, buttonHeight, Color.RED, "HELP");
                    helpButton.drawButton(canvas, drawPaint);

                    // Display the FIRE button
                    MyButton fireButton = new MyButton();
                    fireButton.setTextButton(fireX, buttonRow, buttonWidth, buttonHeight, Color.RED, "FIRE");
                    fireButton.drawButton(canvas, drawPaint);

                    fireDown = 0;
                    for (int i = 0; i < mActivePointers.size(); i++) {
                        PointF point = mActivePointers.valueAt(i);

                        if (point != null) {

                            float x = point.x;
                            float y = point.y - HeightDiff;

                            if (larrowButton.contains((int) x, (int) y)) {
                                if (gunX > speedX)
                                    gunX -= speedX;
                            } else if (rarrowButton.contains((int) x, (int) y)) {
                                if (gunX < ScreenWidth - speedX)
                                    gunX += speedX;
                            } else if (fireButton.contains((int) x, (int) y)) {
                                fireDown = 1;
                            } else if (helpButton.contains((int) x, (int) y)) {
                                gameMode = 3;
                                break;
                            }
                        }
                    }

                    if (fireDown == 0)
                        firedMissile = 0;

                    if (fireDown == 1 && firedMissile == 0) {
                        soundpool.play(missileSound, 1, 1, 0, 0, 1);
                        missileLine.addMissile(new Missile(missilePic, missileSpeed));
                        firedMissile = 1;
                    }

                    canvas.drawBitmap(gun, gunX, gunY, drawPaint);

                    // Check for a button press
                    if ((MyTouch_0 == 0) && (MyTouch == 1)) {

                        // FIRE button has been pressed...
                        // HELP has been pressed.  Change to gameMode = 3
                        if (helpButton.contains((int) MyTouchX, (int) MyTouchY)) {
                            gameMode = 3;
                            MyTouch = 0;
                            MyTouch_0 = 0;
                            break;
                        }
                        MyTouch_0 = 1;
                    }

                    if (MyTouch == 2) {
                        MyTouch_0 = 0;
                    }

                    // Draw all of the missiles
                    missileLine.setLocation(gunY, 50, gunX + (gun.getWidth() / 2) - (missilePic.getWidth() / 2));
                    missileLine.updateMissiles();
                    missileLine.drawRow(canvas, drawPaint);

                    // Draw the score
                    drawScore(canvas, drawPaint);

                    // Check to see if the game time has elapsed
                    // If so, go to the end screen, gameMode = 2
                    if (timerFinished) {
                        timerFinished = false;
                        timerRunning = false;
                        gameMode = 2;
                        MyTouch = 0;
                        MyTouch_0 = 0;
                        countDownTimer.cancel();
                        finalScore = score;
                    }
                    break;


                //    End of game screen. It displays the win or lose picture, and waits for a CONTINUE button
                //    press, which takes you back to the main splash screen, gameMode = 0
                case 2:
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.CENTER);
                    drawPaint.setTextSize(50);

                    // Remove all ships, in case we play again
                    for (int i = 0; i < ROWCOUNT; i++) {
                        ShipRow rw = rows[i];
                        rw.clearShips();
                    }

                    // Remove all missiles, in case we play again
                    missileLine.clearMissiles();

                    // Did we win or lose?
                    if (finalScore <= 0) {
                        //canvas.drawBitmap(kirkhug, ScreenWidth / 2 - (splash.getWidth() / 2), ScreenHeight / 2 - (splash.getHeight() / 2), drawPaint);
                        canvas.drawText("You Lose!!!  Your Score is " + finalScore, ScreenWidth / 2, 200, drawPaint);
                        nextGameMode = 0;
                        score = 0;
                    } else {
                        canvas.drawText("You Win!!!  Your Score is " + finalScore, ScreenWidth / 2, 200, drawPaint);
                        nextGameMode = 4;
                        score = 0;
                    }

                    // Draw the CONTINUE button
                    drawPaint.setTextSize(50);
                    MyButton contButton = new MyButton();
                    contButton.setTextButton(50, ScreenHeight - 150, 500, 90, Color.YELLOW, "Press here to Continue...");
                    contButton.drawButton(canvas, drawPaint);

                    for (int i = 0; i < mActivePointers.size(); i++) {
                        PointF point = mActivePointers.valueAt(i);

                        if (point != null) {

                            float x = point.x;
                            float y = point.y - HeightDiff;

                            if (contButton.contains((int) x, (int) y)) {
                                gameMode = nextGameMode;
                                timerRunning = false;
                                from = 2;
                                break;
                            }
                        }
                    }
                    break;


                //    Help screen.  Displays help text, and wait for the user to press the RESUME key.
                case 3:
                    timerPaused = true;
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.DKGRAY);
                    drawPaint.setColor(Color.WHITE);
                    drawPaint.setTextAlign(Paint.Align.LEFT);
                    drawPaint.setTextSize(20);

                    // Set up help text position and spacing
                    int row = 50, col = RawWidth/2;
                    int lineSpace = RawHeight/10;

                    canvas.drawText("Press the FIRE button to lauch missiles.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("A game runs for 45 seconds.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("If you hit a friendly ship, you lose points.", col, row, drawPaint);
                    row += lineSpace;
                    canvas.drawText("Hit enemy ships to score points.", col, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[2], col, row - (ships[2].getHeight() / 2), drawPaint);
                    canvas.drawText("Balloon is -3 points (you lose points)", col + 150, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[1], col, row - (ships[1].getHeight() / 2), drawPaint);
                    canvas.drawText("Enterprise is -3 points (you lose points)", col + 150, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[6], col, row - (ships[6].getHeight() / 2), drawPaint);
                    canvas.drawText("Borg Ship is 2 points", col + 150, row, drawPaint);
                    row += lineSpace;

                    canvas.drawBitmap(ships[4], col, row - (ships[4].getHeight() / 2), drawPaint);
                    canvas.drawText("Romulan Warbird is 1 point", col + 150, row, drawPaint);
                    row += lineSpace;

                    // Set up the RESUME button
                    drawPaint.setTextSize(50);
                    MyButton retButton = new MyButton();
                    retButton.setTextButton(50, 50, 200, 90, Color.YELLOW, "Resume...");
                    retButton.drawButton(canvas, drawPaint);

                  //  if (MyTouch == 2) {
                        for (int i = 0; i < mActivePointers.size(); i++) {
                            PointF point = mActivePointers.valueAt(i);

                            if (point != null) {

                                float x = point.x;
                                float y = point.y - HeightDiff;

                                if (retButton.contains((int) x, (int) y)) {
                                    gameMode = 1;
                                    from = 3;
                                    break;
                                }
                            }
                        }
                    //}
                    break;
                case 4:
                    drawPaint.setAlpha(255);
                    canvas.drawColor(Color.GRAY);
                    canvas.drawText("Your name:" + HighName, ScreenWidth / 3, ScreenHeight / 6, drawPaint);
                    // Draw a keyboard
                    int keyWidth = (int)((ScreenWidth/14));
                    int keyY = ScreenHeight - keyWidth * 4;
                    int keyX = (ScreenWidth/2) - (keyWidth+5) * 6;
                    kb.drawKeyboard(keyX, keyY, keyWidth, keyWidth, canvas, drawPaint);
                    //for (int i = 0; i < mActivePointers.size(); i++) {

                    if (mActivePointers.size() > 0) {
                        if (keyLock == 1) {
                            if (MyTouch != 2) {
                                //gameMode = 0;
                                break;
                            }
                        }
                        PointF point = mActivePointers.valueAt(0);

                        if (point != null) {
                            keyLock = 1;
                            float x = point.x;
                            float y = point.y - HeightDiff;
                            String t;
                            if ((t = kb.checkKey((int) x, (int) y)) != null) {
                                if (t.equals("SPACE")) {
                                    HighName += " ";
                                }
                                else if (t.equals("DEL")) {
                                    if (HighName.length() > 0)
                                        HighName = HighName.substring(0, HighName.length() - 1);
                                }
                                else if (t.equals("DONE")) {
                                    hs.dbInsert(HighName, finalScore);
                                    gameMode = 0;
                                    break;
                                }
                                else {
                                    HighName += t;
                                }
                            }
                        }
                    }
                    else {
                        keyLock = 0;
                    }
                    break;


                default:
                    break;
            }

            return;

        }



public void drawKeyboard(int x, int y, int scale)
{




}

             // Draw the current score on the game screen
        public void drawScore(Canvas c, Paint p) {
            p.setColor(Color.BLACK);
            p.setTextAlign(Paint.Align.CENTER);
            c.drawText("Score: " + score, scoreX, scoreY, p);
            c.drawText("Time: " + MyTime, timeX, timeY, p);
        }


        // Additional support classes

        // Game timer
      }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            timerFinished = true;
        }

        @Override
        public void onTick(long millisUntilFinished) {
                MyTime = "" + (millisUntilFinished / 1000);
            timerLeft = millisUntilFinished;
        }
    }





    }


