package com.example.jjfinnivan.androidgame;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Missile {
	int x, y, speed;
    boolean visible;
    Bitmap theImage;
    Rect r;
    int index = 0;

    Missile(Bitmap pic, int speed) {
        theImage = pic;
        this.speed = speed;
        visible = false;
        r = new Rect();
    }

    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
        visible = true;
    }

    public void draw(Canvas c, Paint p) {
        if (visible) {
            c.drawBitmap(theImage, x, y, p);
            r.set(x, y, x + theImage.getWidth(), y + theImage.getHeight());
            //c.drawRect(r, butPaint);
        }
    }

    // move the missile.  We are moving up the screen, so we subtract
    public void move() {
        y -= speed;
    }

    public void remove() {
        visible = false;
    }

    public int getY() {
        return y;
    }

    public Rect getRect() {
        return r;
    }
}

