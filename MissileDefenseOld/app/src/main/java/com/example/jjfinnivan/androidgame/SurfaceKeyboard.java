package com.example.jjfinnivan.androidgame;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
/**
 * Created by jjfinnivan on 11/19/15.
 */
public class SurfaceKeyboard {
    MyButton [] keys = new MyButton[29];
    int width;
    int height;

   SurfaceKeyboard() {
       width = 100;
       height = 100;
       //keys = new MyButton[28];
       String myChars = "QWERTYUIOPASDFGHJKLZXCVBNM";

       String c = Character.toString(myChars.charAt(1));


       for (int i = 0; i < 10; i++) {
           keys[i] = new MyButton();
           keys[i].setText(Color.YELLOW, Character.toString(myChars.charAt(i)));
       }

       for (int i = 10; i < 19; i++) {
           keys[i] = new MyButton();
           keys[i].setText(Color.YELLOW, Character.toString(myChars.charAt(i)));
       }

       for (int i = 19; i < 26; i++) {
           keys[i] = new MyButton();
           keys[i].setText(Color.YELLOW, Character.toString(myChars.charAt(i)));
       }
       keys[26] = new MyButton();
       keys[26].setText(Color.YELLOW, "DEL");
       keys[27] = new MyButton();
       keys[27].setText(Color.YELLOW, "SPACE");
       keys[28] = new MyButton();
       keys[28].setText(Color.YELLOW, "DONE");


   }

    void drawKeyboard(int x, int y, int width, int height, Canvas c, Paint p)
    {
        keys[26].setParms(x + (10*(width+5)), y, width*2, height);
        keys[26].drawButton(c, p);

        for (int i = 0; i < 10; i++) {
            keys[i].setParms(x + (i * (width + 5)), y, width, height);
            keys[i].drawButton(c, p);
        }

        x += (width/2);
        y += height + 5;

        for (int i = 10; i < 19; i++) {
            keys[i].setParms(x + ((i - 10) * (width + 5)), y, width, height);
            keys[i].drawButton(c, p);
        }

        keys[28].setParms(x + (9*(width+5)), y, width*2, height);
        keys[28].drawButton(c, p);

        x += (width/2);
        y += height + 5;

        for (int i = 19; i < 26; i++) {
            keys[i].setParms(x + ((i - 19) * (width + 5)), y, width, height);
            keys[i].drawButton(c, p);
        }

        y += height + 5;
        keys[27].setParms(x, y, (width+5)*7, height);
        keys[27].drawButton(c, p);
    }

    String checkKey(int x, int y)
    {
        for(int i = 0; i < 29; i++) {
            if (keys[i].contains(x, y))
                return keys[i].getString();
        }
        return null;

    }
}

