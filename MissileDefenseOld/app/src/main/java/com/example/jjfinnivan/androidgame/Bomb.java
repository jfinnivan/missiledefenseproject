package com.example.jjfinnivan.androidgame;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Bomb {
	int x, y, speed;
    boolean visible;
    Bitmap theImage;
    Bitmap boom;
    Rect r;
    int index = 0;
    double yinc;
    int gunX, gunY;

    Bomb(Bitmap pic, int x, int y, int gunX, int gunY, int speed, Bitmap boom) {
        theImage = pic;
        this.speed = speed;
        visible = false;
        r = new Rect();
        this.gunX = gunX;
        this.gunY = gunY;
        //setPos(x, y);
        this.x = x;
        this.y = y;
        visible = true;
        this.boom = boom;
    }

    public void setPos(int x, int y) {
        //this.x = x;
        //this.y = y;
        visible = true;
    }

    public void draw(Canvas c, Paint p) {
        if (visible) {
            c.drawBitmap(theImage, x, y, p);
            r.set(x, y, x + theImage.getWidth(), y + theImage.getHeight());
            //c.drawRect(r, butPaint);
        }
    }

    // move the bomb.  We are moving up the screen, so we subtract
    public void move() {
        x += speed;
        yinc = (double)speed/((double)(gunX-x)/(double)(gunY-y));
        y = (int)(y+yinc);
    }

    public void remove() {
        visible = false;
    }
    public void explode(Canvas c, Paint p, Bitmap boom1) {
        c.drawBitmap(boom1, x, y, p);
        //boomed = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Rect getRect() {
        return r;
    }
}

