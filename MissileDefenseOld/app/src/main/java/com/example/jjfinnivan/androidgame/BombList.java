package com.example.jjfinnivan.androidgame;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Bitmap;

/**
 * Created by jjfinnivan on 9/19/15.
 */
// This is the list of all bombs
public class BombList {
    int start_x, start_y, end_x, end_y;
    Bomb[] bombs = new Bomb[101];
    int MaxBombs = 100;
    int lastBombIndex;
    int bombCount;
    double angle;
    int maxBombs;
    Rect brect;

    BombList(int maxBombs) {
        bombCount = 0;
        lastBombIndex = 0;
        this.maxBombs = maxBombs;

    }

    public int getCount()
    {
        return bombCount;

    }

    public void setLocation(int start_x, int starty_y, int end_x, int end_y) {
        this.start_x = start_x;
        this.start_y = start_y;
        this.end_x = end_x;
        this.end_y = end_y;
        angle = Math.atan((end_x-start_x)/(end_y-start_y));

    }

    // draw all bombs in the list
    public void drawBombs(Canvas c, Paint p) {
        //c.drawBitmap(gun, x - (gun.getWidth() / 4), start, p);

        for (int i = 0; i < MaxBombs; i++) {
            if (bombs[i] != null) {
                bombs[i].draw(c, p);
            }
        }
    }

    // add a new bomb
    public void addBomb(Bomb m) {
        for (int i = 0; i < MaxBombs; i++) {
            if (bombs[i] == null) {
                bombs[i] = m;
                bombs[i].index = i;
                bombs[i].setPos(start_x, start_y);
                bombCount++;
                return;
            }
        }
    }

    // move each bomb.  If one falls off the end, remove it
    public void updateBombs(Canvas c, Paint p, int gunX, int gunY, Bitmap gun, Bitmap boom) {
        for (int i = 0; i < MaxBombs; i++) {
            if (bombs[i] != null) {
                bombs[i].move();

                if (bombs[i].getY() >= gunY) {
                    bombs[i].remove();
                    bombCount--;
                    bombs[i] = null;
                }
                else {
                    // check to see if a bomb has hit the gun
                    Rect s = bombs[i].getRect();
                    brect = new Rect();
                    brect.set(gunX, gunY, gunX + gun.getWidth(), gunY + gun.getHeight());
                    if ((s != null) && s.intersect(brect) == true) {
                        //score += ships[i].getScore();
                        bombs[i].explode(c, p, boom);
                        //sound.play(explode, 5, 5, 0, 0, 1);
                    }
                }
            }
        }
    }

    // Check to see if we have collided with the gun
    public boolean checkForCollision(Rect sr) {
        for (int i = 0; i < MaxBombs; i++) {
            if (bombs[i] != null) {
                if (Rect.intersects(sr, bombs[i].getRect())) {
                    bombs[i].remove();
                    bombs[i] = null;
                    bombCount--;
                    return true;
                }
            }
        }
        return false;
    }

    // Clear all bombs
    public void clearBombs() {
        for (int i = 0; i < MaxBombs; i++) {
            bombs[i] = null;
        }
    }

}

